<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta name="language" content="en" />
  	<meta name="robots" content="none" /> 
	<!-- blueprint CSS framework -->
	
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/main.css" type="text/css" media="screen, projection">
	<script src="http://localhost/jquery.min.js"></script>
	<script type="text/javascript" defer src="js/select_language.js"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div id="wrapper">

<header>
		<div class="left_sidebar">
			<div class="logo">
				<img alt="logo" src="../images/city8_tattoo_logo.png" height="90" />
			</div>
		</div>
		<div class="center">
			<div class="conteiner">
				<div id="top_menu">
						<?php 
						$blogact=0;
						$cataction=0;
						$seract=0;
						$filesaction=0;
						$slideract=0;
						if(isset(Yii::app()->controller->module->id)){
							//catalog
							if(Yii::app()->controller->module->id=='catalog'){
								$cataction=1;
							}
							//services
							if(Yii::app()->controller->module->id=='services'){
								$seract=1;
							}
							
							
							//blog
							if(Yii::app()->controller->module->id=='blog'){
								$blogact=1;
							}
						} else {
							//files
							if(Yii::app()->controller->id=='files'){
								$filesaction=1;
							}
							//slider
							if(Yii::app()->controller->id=='slider'){
								$slideract=1;
							}
						}
						$this->widget('zii.widgets.CMenuAdmin',array(
			'items'=>array(
				array('label'=>'Мастера', 'url'=>array('/catalog/category/index'), 'itemOptions'=>array('class'=>'catalog block btn'), 'active'=>$cataction),
				//array('label'=>'Услуги', 'url'=>array('/services/services/index'), 'itemOptions'=>array('class'=>'news block btn'), 'active'=>$seract),
				//array('label'=>'Файлы', 'url'=>array('/files/index'), 'itemOptions'=>array('class'=>'filemanager block btn'), 'active'=>$filesaction),
				array('label'=>'Статьи', 'url'=>array('/blog/article/index'), 
				'active'=>$blogact,
				'itemOptions'=>array('class'=>'articles block btn')),
				array('label'=>'Слайдер', 'url'=>array('/slider/index'), 'itemOptions'=>array('class'=>'slider block btn'), 'active'=>$slideract),
				array('label'=>'Настройки', 'url'=>array('/site/config'), 'itemOptions'=>array('class'=>'settings block btn')),
				array('label'=>'Выход', 'url'=>array('/site/logout'), 'itemOptions'=>array('class'=>'logout block btn')),
			),
		)); ?>
				</div>
			</div>
		</div>
	</header>
	<?php /*?>
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
*/?>
	<div id="content">
		<?php echo $content; ?>
	</div>
	
</div><!-- page -->
<div id="footer">
	5345043@gmail.com &copy; 2013 Версия 0.5
</div><!-- footer -->
</body>
</html>
