function backtotop(){
	this.window_height = 0;//высота окна
	this.header_height = 0;//высота header
	this.min_height = 1000;//минимальная высота, прик которой хедар фиксируется
	this.top_button = 0;//top для кнопки backtotop
	this.scroll_top = 0;//отступ body2
	this.view = 0;//флаг отображения кнопки
	self = this;
	
	
	this.setHeaderHeight = function(){
		this.header_height = $('#wrapper_header').css('height');
		this.header_height = this.header_height.replace("px", "");
	};
	
	this.setScrollTop = function(){
		this.scroll_top = $('#body2').scrollTop();
		this.setView();
	};
	
	this.setView = function(){
		if( this.scroll_top  > 200 ){
			if( this.view != 1 ){
				this.view = 1;
				$('.back_to_top').fadeTo(500, 1);
			}
		}else{
			if( this.view != 0 ){
				this.view = 0;
				$('.back_to_top').fadeTo(500, 0);
				this.top_button = 0;
			}
		}
		
		if ( this.window_height < this.min_height ){
			this.setTopButton();
		}
	};
	
	this.setTopButton = function(){
		if (this.scroll_top <= this.header_height){
			this.top_button = this.header_height - this.scroll_top
			$('.back_to_top').css('top', this.top_button);
		}
	};
	
	
	this.setWindowHeight = function(){
		this.window_height = document.documentElement.clientHeight;
		
		if ( this.window_height >= this.min_height ){
			$('#wrapper_header').width( $('#wrapper').width() );
		}
	};
	
	this.setWindowHeight();
	this.setHeaderHeight();
	this.setScrollTop();
	
	$('#body2').scroll(function () {
		self.setScrollTop();
	});
	
	$('.back_to_top').on('click', function(){
		$("#body2").animate({"scrollTop":0},"fast");
	});
}



$(window).load(function() {
	mybacktottop = new backtotop();
});


