$(function() {  
	App.Models.BackToTop = Backbone.Model.extend({
		defaults: {
			"header":	'scroll',
			"top":		0,
			"view":		0
		}
    	
	});
	
	var BackToTopModel = new App.Models.BackToTop;
	
	console.log(BackToTopModel);
});