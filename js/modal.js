var Modal = function(){
	var self = this;
	var template = function(id) {
        return _.template( $('#' + id).html() );
    };
    
    this.ModalModel = Backbone.Model.extend({
    	initialize: function() {
    		this.set( 'modal_id', 'modal_' + this.cid );
    	},
    	
    	defaults: {
    		close	 : true,
    	}
	});
    
    var ModalView = Backbone.View.extend({
    	tagName: 'div',
    	className: 'm',
    	id: function() {
    		return this.model.get('modal_id');
    	},
    	
    	template: template('modalTemplate'),
    	
    	initialize: function() {
    		this.model.on('destroy', this.closeWindow, this);
    		this.model.on('change', this.showHideClose, this); 
    	},
    	
    	render: function() {
    		var template = this.template(this.model.toJSON());
            this.$el.html( template );
            this.showClose();
            return this;
    	},
    	
    	events: {
    	    'click .close': 'deleteModal',   
    	},
    	
    	deleteModal: function  () {
    	    this.model.destroy(this.model);
    	},
    	
    	showClose: function() {
    		var m_close = $('<div>', {
    			class: 'close'
    		});
    		this.$el.find(".m_m_m_header").append(m_close);
    	},
    	
    	hideClose: function() {
    		this.$el.find(".m_m_m_header .close").remove();
    	},
    	
    	showHideClose: function(){
    		if (this.model.get('close') === true){
    			this.showClose();
    		}else{
    			this.hideClose();
    		}
    	},
    	
    	setClose: function() {
    		var value;
    		if (this.model.get('close') === true){
    			value = false;
    		}else{
    			value = true;
    		}
    		this.model.set('close', value);
    	},
    	
    	closeWindow : function() {
    		this.$el.remove(); 
    	}
    	
    });
    
    var ModalCollection = Backbone.Collection.extend({
        model: self.ModalModel
    });
    
    var ModalsView = Backbone.View.extend({
        tagName: 'div',
        className: 'modals',
        
        initialize: function() {
        	$('body').append(this.render().el);   
        	this.collection.on('add', this.addOne, this );
        },
        
        render: function() {
            this.collection.each(this.addOne, this);
            return this;
        },
        
        addOne: function(modal) {
            // создавать новый дочерний вид
            var modalView = new ModalView({ model: modal });
            // добавлять его в корневой элемент
            this.$el.append(modalView.render().el);
        }
    });
    
	var ModalsCollection = new ModalCollection([ ]);   
    
    this.modalsView = new ModalsView({ collection: ModalsCollection});
    

    this.AddModal = function(param) {
    	this.modalsView.collection.add(param);
    };
};
