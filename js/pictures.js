$(function(){
	window.App = {
		Models: {},
		Collections: {},
		Views:{},
		Router:{},
	};
	Backbone.history.start();
	var vent = _.extend({}, Backbone.Events);
	
	window.template = function(id) {
        return _.template( $('#' + id).html() );
    };
    
	/**
	 * @type App.Models.Picture
	 */
    App.Models.Picture = Backbone.Model.extend({
		/**
		 * @memberOf App.Models.Picture
		 */
		defaults: {
			id : null,
			name_lang1 : null,
			name_lang2 : null,
			name_lang3 : null,
			master_lang1 : null,
			master_lang2 : null,
			master_lang3 : null,
			tatoo_image : null,
			date_create : null,
			photo_type : null,
			show : false
		},

	});
    
    /**
	 * @type App.Models.Page
	 */
    App.Models.Page = Backbone.Model.extend({
		defaults: {
			count: 1,//количество загруженных страниц
			ajax: false,//флаг подгрузки
			lang: 1,
			master: null,
			type: null,
			all_pages: false, // флаг, который показывает все ли страницы загружены
		},
		/**
		 * @memberOf App.Models.Page
		 */
		initialize: function(){
			this.set('lang', $('nav.language a.active').index()+1);
			if( $('nav.filters a.active').index() != 0 ){
				this.set('type', $('nav.filters a.active').index());
			}
			if( $('#master').length > 0 ){
				this.set('master', $('#master').attr(''));
			}
			
		},
		/**
		 * @memberOf App.Models.Page
		 */
	});
    
    var pageModel = new App.Models.Page();
    console.log(pageModel);
    
    /**
	 * @type App.Views.Page
	 */
    App.Views.Page = Backbone.View.extend({
    	/**
    	 * @memberOf App.Views.Page
    	 */
    	model: pageModel,
    	
    	initialize: function(){
            this.setElement('.images.sens_pictures');
    		vent.on('page:show', this.show, this);
            var ajaxLoader = new App.Views.ajaxLoader
    	},
    	
    	renderAjaxLoader: function(){

    		this.$el.append( '<div class="ajax_loader">Please, wait...</div>' );
    		return this;
    	},
    	
    	removeAjaxLoader: function() {
            this.$el.remove(); 
        },
        
        show: function(){
        	alert('show');
        },
        
        ajaxPage: function(){
        	this.model.set('count', this.model.get('count') + 1);
        	this.model.set('ajax', true);
        }, 
        
        setFlag: function(){
        	this.model.set('ajax', false);
        	this.remove();
        },
    });
    
    var pageView = new App.Views.Page();
    console.log(pageView);
    
	/**
	 * @type App.Views.Picture
	 */
	App.Views.Picture = Backbone.View.extend({
		/**
		 * @memberOf App.Views.Picture
		 */
		tagName: 'a',
	   	className: 'show_pictures',
	   	
	    	
	   	template: template('pictureTemplate'),
	    	
	    initialize: function() {
	    	$(this.el).attr('href', '#' + this.model.id)
	    },
	    
	    events:{
            'click': 'viewFoto',
        },
        
	    //рендер элемента при ajax	
	    render: function() {
	    	var template = this.template(this.model.toJSON());
	        this.$el.html( template );
	        return this;
	    },
	    
	    //рендер элемента при парсинге страницы
	    render2: function(){
	    	this.setElement('#content .images.sens_pictures a:nth-child('+ this.model.collection.length +')');
	    },
	    
	    viewFoto: function(event) {
	    	alert('просмотр фото ' + this.model.get('name_lang1'))
	    },
	    	


	});
	
	
	/**
	 * @type App.Collections.Picture
	 */
    App.Collections.Picture = Backbone.Collection.extend({
    	/**
		 * @memberOf App.Collections.Picture
		 */
        model: App.Models.Picture
    });
    
    
    /**
	 * @type App.Views.Pictures
	 */
    App.Views.Pictures = Backbone.View.extend({
    	/**
		 * @memberOf App.Views.Pictures
		 */
        initialize: function() {
        	this.setElement('#content .images.sens_pictures').render().el;   
        	this.collection.on('add', this.addOne, this );
        	this.startCollection();
        },
        
        render: function() {
        	this.collection.each(this.addOne, this);
	        return this;
        },
        
        addOne: function(picture) {      	
           // создавать новый дочерний вид
            var pictureView = new App.Views.Picture({ model: picture });
            if( pictureView.model.attributes.ajax === true){
            	// добавлять его в корневой элемент
            	this.$el.append(pictureView.render().el);
            }else{
            	this.$el.append(pictureView.render2());
            }
        },

        
        //парсим начальную коллекцию изображений
        startCollection: function(){
        	var length = $('a.show_pictures').size();
        	var master_name ,image_name, date ,href ,src ;
        	for ( var i = 1; i <= length; i++ ){
        	
        		master_name = $('a.show_pictures:nth-child(' + i + ') .master_name').text();
        		image_name = $('a.show_pictures:nth-child(' + i + ') .name').text();
        		image_name = image_name.replace('«', '');
        		image_name = image_name.replace('»', '');
           		date = $('a.show_pictures:nth-child(' + i + ') .date').text();
           		src = $('a.show_pictures:nth-child(' + i + ') .img img').attr('src');
           		id = $('a.show_pictures:nth-child(' + i + ')').attr('href');
           		id = id.replace('#', '');
           		this.collection.add({
    					ajax : false,
    					id : id,
    					name_lang1 : image_name,
    					name_lang2 : image_name,
    					name_lang3 : image_name,
    					master_lang1 : master_name,
    					master_lang2 : master_name,
    					master_lang3 : master_name,
    					tatoo_image : src,
    					date_create : date,
    					photo_type : pageModel.get('type')
    				});
        		
        	};
        },
        
        getPicturesAjax: function(page){
        	this.$el.append(pageView.render().el);
        	pageView.ajaxPage();
        	console.log('http://localhost/tatoo2/get-pictures/' + pageModel.get('count'))
        	var url = '';
        	if( pageModel.get('type') !== null ){
        		url += '/type/' + pageModel.get('type');
        	}
        	if( pageModel.get('master') !== null ){
        		url += '/master/' + pageModel.get('master');
        	}
        	jQuery.ajax({
        		'type':'POST',
        		'dataType':'json',
        		'url': 'http://localhost/tatoo2/get-pictures/' + pageModel.get('count') + url,
        		'success':function(data) {
        			if( $.isEmptyObject(data) ){
        				alert('pusto')
        				console.log(data)
        			}else{
	        			for (var i=0; i<6; i++) {
	        				picturesView.collection.add(data[i])
	        			}
        			}
        			
        		}
        	});
        	pageView.setFlag();
        }
    });
   
    
    window.PicturesCollection = new App.Collections.Picture();
    
    var picturesView = new App.Views.Pictures({ collection: PicturesCollection});
    
    console.log( picturesView);
    console.log( picturesView.collection.models[0]);
    
    /**
	 * @type App.Models.Modal
	 */
    App.Models.Modal = Backbone.Model.extend({
    	/**
		 * @memberOf App.Models.Modal
		 */
    	initialize: function() {
    		this.set( 'modal_id', 'modal_' + this.cid );
    	},
    	
    	defaults: {
    		close: true,
    		windowShow : true
    	}
    });
    /**
	 * @type App.Views.ModalHeader
	 */
    App.Views.ModalHeader = Backbone.View.extend({
    	/**
		 * @memberOf App.Views.ModalHeader
		 */
    	tagName: 'nav',
    	className: 'soc',
    	
    	template: template('modalTemplate'),
    	
    	render: function() {
    		var template = this.template();
            this.$el.html( template );
            return this;
    	},
    });

    /**
	 * @type App.Views.Modal
	 */
    App.Views.Modal = Backbone.View.extend({
    	/**
		 * @memberOf App.Views.Modal
		 */
    	tagName: 'div',
    	className: 'm',
    	id: function() {
    		return this.model.get('modal_id');
    	},

    	template: template('modalTemplate'),
    	
    	initialize: function() {
    		if( this.model.get('windowShow') === true){
    			this.$el.css('display', 'block');
    		}else{
    			this.$el.css('display', 'none');
    		}
    	},
    	
    	render: function() {
    		var template = this.template(this.model.toJSON());
            this.$el.html( template );
            this.showClose();
            return this;
    	},
    	
    	events: {
    	    'click .close': 'showCloseModal',   
    	},
    	
    	deleteModal: function  () {
    	    this.model.destroy(this.model);
    	},
    	
    	showClose: function() {
    		var m_close = $('<div>', {
    			class: 'close'
    		});
    		this.$el.find(".m_m_m_header").append(m_close);
    	},
    	
    	hideClose: function() {
    		this.$el.find(".m_m_m_header .close").remove();
    	},
    	
    	showHideClose: function(){
    		if (this.model.get('close') === true){
    			this.showClose();
    		}else{
    			this.hideClose();
    		}
    	},
    	
    	setClose: function() {
    		if (this.model.get('close') === true){
    			value = false;
    		}else{
    			value = true;
    		}
    		this.model.set('close', value);
    	},
    	
    	showCloseModal : function() {
    		var self = this;
    		if( this.model.get('windowShow') === false){
    			this.$el.fadeIn(300, function(){
    				self.model.set('windowShow', true);
    			});
    		}else{
    			this.$el.fadeOut(300, function(){
    				self.model.set('windowShow', false);
    			});
    		}
    		
    	}
    	
    });
   
    	
    /**
    * @type App.Collections.Modal
    */
    App.Collections.Modal = Backbone.Collection.extend({
    	/**
		 * @memberOf App.Collections.Modal
		 */
        model: App.Models.Modal
    });
    
    /**
     * @type App.Views.Modals
     */
    App.Views.Modals = Backbone.View.extend({
    	/**
		 * @memberOf App.Views.Modals
		 */
        tagName: 'div',
        className: 'modals',
        
        initialize: function() {
        	$('body').append(this.render().el);   
        	this.collection.on('add', this.addOne, this );
        },
        
        render: function() {
            this.collection.each(this.addOne, this);
            return this;
        },

    	
    	render_header: function(modal){
    		var modalHeader = new App.Views.ModalHeader();
    		console.log(modalHeader)
    		this.$('#modal_' +this.collection.get(modal).cid + ' .m_m_m_header').prepend(modalHeader.render())
    	},
    	
    	render_content_prepend: function(modal, content){
    		this.$('#modal_' +this.collection.get(modal).cid + ' .m_m_m_content').prepend(content)
    	},
    	
    	render_content_append: function(modal, content){
    		this.$('#modal_' +this.collection.get(modal).cid + ' .m_m_m_content').append(content)
    	},
    	
    	render_footer_prepend: function(modal, content){
    		this.$('#modal_' +this.collection.get(modal).cid + ' .m_m_m_footer').prepend(content)
    	},
    	
    	render_footer_append: function(modal, content){
    		this.$('#modal_' +this.collection.get(modal).cid + ' .m_m_m_footer').append(content)
    	},
    	
        addOne: function(modal) {
            // создавать новый дочерний вид
            var modalView = new App.Views.Modal({ model: modal });
            // добавлять его в корневой элемент
            this.$el.append(modalView.render().el);
        }
    });
    
    /**
     * @type App.Router
     */
    App.Router = Backbone.Router.extend({
    	routes: {
            ''                       : 'index',
            ':id'        			 : 'showPicture',
            '*other'                 : 'default'
        },
     
        index: function() {
            console.log('Всем привет от индексного роута!');
        },
        
        showPicture: function(id) {
            console.log(picturesView.collection.get(id));
        },
     
     
        default: function(other) {
            alert('Хммм...вы уверены, что вы попали туда куда хотели? Вы находитесь на роуте ' + other);
        }
        /**
		 * @memberOf App.Router
		 */
    });
    
   
    new App.Router();
    //console.log(App.Router());
    var picturesModel = new App.Models.Modal({id: 'picturesModel'});
    window.ModalsCollection = new App.Collections.Modal(picturesModel);   
    var modalsView = new App.Views.Modals({ collection: ModalsCollection});
    console.log(modalsView)
  //  console.log(modalsView.collection.get('picturesModel'))
    modalsView.render_header('picturesModel');
    modalsView.render_content_append('picturesModel','conent');
    modalsView.render_footer_append('picturesModel', 'footer');
    
    

    
    $(document).scroll(function () {
	    	var scroll_top = $(document).scrollTop();//высота прокрученной области
	    	var page_height = $(document).height();//высота всей страницы
	    	var wind_height = $(window).height();//высота окна браузера
	    	  //если непрокрученной области больше чем высота окна обраузера, то подгружаем след. объекты
	    	
	    	if((page_height - scroll_top) < wind_height + 100){
	    	   //pageModel.set( 'count', pageModel.get('count') + 1 );
	    	   if( pageModel.get('ajax') === false){
	    		   picturesView.getPicturesAjax();
	    	   }else{
	    		   console.log(pageModel.get('ajax'))
	    	   }
	    	}	
    });
});