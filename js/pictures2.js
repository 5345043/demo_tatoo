$(function(){
	window.App = {
		Models: {},
		Collections: {},
		Views:{},
		Router:{},
	};
	window.fetch_timer = null;
	window.picture_now = null;
    window.base_href = 'http://localhost/tatoo4/';

	var vent = _.extend({}, Backbone.Events);

	window.template = function(id) {
		return _.template( $('#' + id).html() );
	};

	App.Router = Backbone.Router.extend({
        routes: {
            ''     : 'index',
            ':id' : 'show'
        },
     
        index: function() {
            console.log('Hello world!!!');   
        },

        showPictures: function(id){
			if( ! $.isEmptyObject(window.PicturesView.collection.get(id)) ){
				var showNow = PicturesCollection.find( function( PicturesCollection ) {
					return PicturesCollection.get("show") === true;
				});
				if( ! $.isEmptyObject(showNow) ){
		    		showNow.set('show', false);
		    	}
				//если модальное окно не открыто
				if( window.PageView.model.get('show_pictures') === false ){
					window.PageView.model.set('show_pictures', true);
					}

					if( window.Modal.model.get('open') === false ){
						window.Modal.model.set('open', true);
					}
					window.Modal.model.set('now_image', id);
					window.PicturesView.collection.get(id).set('show', true);
			       	//console.log(window.PicturesView.collection.get(id) );
			    }else{
			  	location.replace( window.base_href + '404' );
			}
        },	
     
        show: function(id) {
        	var self = this;
        	window.picture_now = id;
			$(document).ready(function($) {
				//console.log(window.fetch_timer)
				if( window.PageView.model.get('pictures_fetch') === false ){
					window.fetch_timer = setTimeout(function(){
						//если модель существует
						self.showPictures(id)
			    	},30000);
				}else{
					self.showPictures(id);
				}
			});
			
               
        }
    });
    
    window.Router = new App.Router();

    App.Models.Modal = Backbone.Model.extend({
    	defaults:{
    		open: true, // флаг просмотрщика display: none/block
    		now_image: false,
    		next_image: false,
    		last_image: false,
    	}
    });

    App.Models.ModalNavigation = Backbone.Model.extend({
    	defaults:{
    		next_image: null,
    		last_image: null,
    	}
    });

	App.Models.Picture = Backbone.Model.extend({
		defaults: {
			id : null,
			name_lang1 : null,
			name_lang2 : null,
			name_lang3 : null,
			master_lang1 : null,
			master_lang2 : null,
			master_lang3 : null,
			master_url : null,
			date_create : null,
			photo_type : null,
			show : false, //отрендерино изображение в просмотрщике или нет
			this_render: false //отрендерино изображение в просмотрщике или нет
		},


        initialize: function(){
        	if( ! $.isEmptyObject(window.MastersView) ){        		
	            this.set('master_lang1', window.MastersView.collection.get(this.get('category_id')).get('name_lang1'));
	            this.set('master_lang2', window.MastersView.collection.get(this.get('category_id')).get('name_lang2'));
	            this.set('master_lang3', window.MastersView.collection.get(this.get('category_id')).get('name_lang3'));
	            this.set('master_url', window.MastersView.collection.get(this.get('category_id')).get('url'));
       		}
        },

        next_image: function () {
		    if (this.collection) {
		    	if( ! $.isEmptyObject( this.collection.at( this.collection.indexOf(this) + 1) ) ){
					return this.collection.at(this.collection.indexOf(this) + 1);
		    	}else{
		    		return this.collection.at(0);
		    	}
		        
		    }
		},

		last_image: function () {
		    if (this.collection) {

		    	if( ! $.isEmptyObject( this.collection.at( this.collection.indexOf(this) - 1) ) ){
					return this.collection.at(this.collection.indexOf(this) - 1);
		    	}else{
					return this.collection.at(this.collection.length-1);
		    	}
		        
		    }
		},

	});

	App.Models.Master = Backbone.Model.extend({});

	App.Models.Page = Backbone.Model.extend({
		defaults:{
			id: 'page',
			page: 1, // количество отрендеренных страниц фото
			count_pictures: 6, // количество фото, которые рендерятся при скролле вниз
			scrollbottom_render: false, // флаг, рендерить ли фото при скролле вниз
			render_now: false, // флаг, рендириться в данный момент что-нибудь
			render_all: false, //флаг - все ли изображения отрендерины
			lang: 1, // язык страницы
			master: null, //страница мастера
			type: null, // тип фото
			pictures_fetch: false, // флаг пассивной загрузки
			show_pictures: false // флаг отрендерен ли просмотр фото 
		},

		initialize: function(){
			this.set('lang', $('nav.language a.active').index()+1);
            if( $('nav.filters a.active').index() != 0 ){
    			if( $('nav.filters').length > 0 ){
                    if( $('nav.filters a.active').index() != 0){
    				    this.set('type', $('nav.filters a.active').index());
                    }
    			}
            }
			if( $('#master').length > 0 ){
				this.set('master', $('#master').attr('data-master'));
			}
			
		},
	});



	App.Views.Page = Backbone.View.extend({	
		model: App.Models.Page,
		
    	initialize: function() {
    		this.setElement('.images.sens_pictures');
			this.model.on('change:render_now', this.toogleAjaxLoader, this );
			this.model.on('change:show_pictures', this.toogleShowPictures, this );
    	},

        toogleAjaxLoader: function() {
        	if ( this.model.get('render_now') === true ){   
        	    if ( this.model.get('render_all') === false ){  		
        			this.$el.append(window.ajaxLoader.render().el);
        			this.addPictures();      		
        		}
        	}else{
        		this.$(window.ajaxLoader.el).remove();
        	}
        	
        },

        addPictures: function(){
        	var start = this.model.get('page') * this.model.get('count_pictures');
        	this.model.set( 'page', this.model.get('page') + 1 );
        	var end = this.model.get('page') * this.model.get('count_pictures');
        	window.PicturesView.render_page(start, end);
        },

        toogleShowPictures: function(){
        	if ( this.model.get('show_pictures') === true){
        		$('body').css('overflow','hidden');
        		$('body').append(window.Modal.render().el);
        	}else{
        		window.Modal.remove();
        	}
        },
	});

	window.ModalModel = new App.Models.Modal();

    App.Views.Modal = Backbone.View.extend({
    	className: 'm',
    	model: window.ModalModel,
    	tagName: 'div',
    	template: template('modalTemplate'),

    	events:{
            'click .close': 'remove',
        },

    	render: function(){
    		var template = this.template(this.model.toJSON());
    		this.$el.html( template );
    		this.$('.m_m_m_content').append(window.ModalNavigationView.render().el);
    		this.$('.m_m_m_content').append(window.ModalContent.el);
    		this.$('.m_m_m_footer').append(window.ModalFooter.el);
    		//console.log();
            return this;
    	},

    	close: function(){		
			this.$el.fadeOut(100, function(){
				$('body').css('overflow', 'visible');
			}); 
    	},

    	initialize: function(){
    		this.model.on('change:open', this.toogleShow, this );
    		this.model.on('change:now_image', this.addImage, this );
    	},

    	show: function(){
    		$('body').css('overflow', 'hidden');
    		this.$el.fadeIn(500); 
    	},

    	toogleShow: function(){
    		if( this.model.get('open') === true ){
    			this.show();
    		}else{
    			this.close();
    		}
    	},

    	remove: function(){
    		this.model.set('open', false);
/*    		var showNow = PicturesCollection.find( function( PicturesCollection ) {
				return PicturesCollection.get("show") === true;
			});*/
    		//showNow.set('show', false);
    		//console.log(this.model)
    	},

    	addImage: function(){
    		var id = this.model.get('now_image');
    		window.ModalContent.addImage( window.PicturesView.collection.get(id) );
    		window.ModalFooter.textImage( window.PicturesView.collection.get(id) );
    	}



    });

    window.ModalNavigation = new App.Models.ModalNavigation();
   //console.log(window.ModalNavigation)

    App.Views.ModalNavigation = Backbone.View.extend({
    	tagName: 'div',
    	className: 'photo_navigation',
    	template: template('modalContentNavigation'),


    	render: function(){
    		var template = this.template(this.model.toJSON());
            this.$el.html( template );
           
            return this;
    	},

    	initialize: function(){
    		this.model.on('change:next_image', this.render, this );
    	},

    });

    window.ModalNavigationView = new App.Views.ModalNavigation({ model:window.ModalNavigation });
    //console.log(window.ModalNavigationView.model);



    window.Modal = new App.Views.Modal();
    //console.log(window.Modal.model)

    App.Views.ModalContentImage = Backbone.View.extend({   	
    	
    	tagName: 'div',
    	template: template('modalContentImage'),
    	/*id: function(){
    		return this.model.cid;
    	},*/

    	initialize: function(){
    		this.model.on('change:show', this.toogleShow, this );
    	},

    	render: function(){
    		var template = this.template(this.model.toJSON());
            this.$el.html( template );
            return this;
    	},

    	toogleShow: function(){
    		if( this.model.get('show') === true ){
    			this.$el.addClass('show');
    		}else{
    			this.$el.removeClass('show');
    		}
    	},
    });

    App.Views.ModalContents = Backbone.View.extend({ 
    	tagName: 'div',
    	className: 'view_image',
    	initialize: function(){

    	},

    	addImage: function(picture) {   
	        var nowImage = new App.Views.ModalContentImage({ model: picture });
	        if ( nowImage.model.get('this_render') === false ){
	        	this.$el.append( nowImage.render().$el.addClass('show') );
	        	nowImage.model.set('this_render', true);
	        }

	        if ( nowImage.model.next_image().get('this_render') === false ){
	        	var nextImage = new App.Views.ModalContentImage({ model: nowImage.model.next_image() });
	        	this.$el.append( nextImage.render().el );
	        	nowImage.model.next_image().set('this_render', true);
	        }

	        if ( nowImage.model.last_image().get('this_render') === false ){
	        	var lastImage = new App.Views.ModalContentImage({ model: nowImage.model.last_image() });
	        	this.$el.prepend( lastImage.render().el );
	        	nowImage.model.last_image().set('this_render', true ); 
	        }

	        window.Modal.model.set('last_image', nowImage.model.last_image().get('id'));
	        window.Modal.model.set('next_image', nowImage.model.next_image().get('id'));
	        window.ModalNavigationView.model.set('last_image', nowImage.model.last_image().get('id'));
	        window.ModalNavigationView.model.set('next_image', nowImage.model.next_image().get('id'));
        },
    });

	App.Views.ModalFooterImage = Backbone.View.extend({
		tagName: 'div',
		className: 'photo',
		template: template('modalFooterText'),

		initialize: function(){
    		this.model.on('change:show', this.remove, this );
    	},

		render: function(){
			
    			var template = this.template(this.model.toJSON());
           		this.$el.html( template );
            	return this;
           
    	},

    	remove: function(){
    		if ( this.model.get('show') === false){
    			this.$el.remove();
    		}
    	}
	});

	App.Views.ModalFooter = Backbone.View.extend({
		tagName: 'div',
		className: 'photo_atributes',
		initialize: function(){
			//console.log(this)
		},
    	
    	textImage: function(picture) {   
	        var nowImage = new App.Views.ModalFooterImage({ model: picture });
	        
	        this.$el.append( nowImage.render().el );
	    }
	});

    

	App.Views.AjaxLoader = Backbone.View.extend({
		tagName: 'div',
		className: 'conteiner',
		template: template('ajaxLoader'),	

		render: function() {
    		var template = this.template();
            this.$el.html( template );
            return this;
    	}
	        
	});

	
	
	App.Views.Picture = Backbone.View.extend({
		tagName: 'a',
	   	className: 'show_pictures',
	   	
	   	template: template('pictureTemplate'),
	    	
	    initialize: function() {
	    	$(this.el).attr('href', '#' + this.model.id)
	    },
	    
	    events:{
            'click': 'viewFoto',
        },
        
	    //рендер элемента при ajax	
	    render: function() {
	    	var template = this.template(this.model.toJSON());
	        this.$el.html( template );
	        return this;
	    },
	    
	
	    viewFoto: function(event) {
	    	//alert('просмотр фото ' + this.model.get('name_lang1'))
	    },
	});

	App.Collections.Master = Backbone.Collection.extend({
        model: App.Models.Master,
    });

    App.Views.Masters = Backbone.View.extend({});    

	App.Collections.Picture = Backbone.Collection.extend({
        model: App.Models.Picture,
        url: function(){
        	var link = window.base_href + 'get-pictures/';
        	if ( window.PageView.model.get('master') !== null ){
        		link += 'master/' + window.PageView.model.get('master') + '/';
        	} 
        	if ( window.PageView.model.get('type') !== null ){
        		link += 'type/' + window.PageView.model.get('type') + '/';
        	} 
        	return link;
        },	
        initialize: function(){
        	this.fetch().complete(function(){
				window.PageView.model.set('pictures_fetch', true);
				clearTimeout(window.fetch_timer)
				window.Router.navigate('', {trigger: true});
				window.Router.navigate(window.picture_now, {trigger: true});
			});
        }
    });

    App.Views.Pictures = Backbone.View.extend({

        initialize: function() {
        	this.setElement('#content .images.sens_pictures').render().el;   
        	this.collection.on('reset', this.addOne, this );
        },

        firstRender: function(){
        	console.log(1)
        	console.log(this.collection);
        },
        
        render: function() {
        	this.collection.each(this.addOne, this);        	
	        return this;
        },

        render_page: function(start, end){

        	for( ; start < end; start++ ){
        		if( ! $.isEmptyObject(this.collection.at(start)) ){

        			this.addOne(this.collection.at(start));
        		}else{
        			PageView.model.set('render_all', true);
        		}
        	}
        	PageView.model.set('render_now', false);
        	return this;
        },
        
        addOne: function(picture) {   
	        var pictureView = new App.Views.Picture({ model: picture });
	        this.$el.append( pictureView.render().el );        	
        },

    });  

    

	
	
	$(document).ready(function($) {
		
		$(document).scroll(function () {
		    var scroll_top = $(document).scrollTop();//высота прокрученной области
		    var page_height = $(document).height();//высота всей страницы
		    var wind_height = $(window).height();//высота окна браузера
		    //если непрокрученной области больше чем высота окна обраузера, то подгружаем след. объекты
            console.log(window.PageView.model.get( 'scrollbottom_render' ))
		    if( window.PageView.model.get( 'scrollbottom_render' ) === true ) {
		    	if ( window.PageView.model.get('render_now') === false ) {
				    if( (page_height - scroll_top) < wind_height + 100 ) {
				       window.PageView.model.set('render_now', true);
				    }	
				}
		    }
	    });
    });

	window.ajaxLoader = new App.Views.AjaxLoader();
    Backbone.history.start(); 
});