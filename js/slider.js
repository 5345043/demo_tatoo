


function slider(slider_id, mask, step, active_elements, speed, indicator){
	this.slider_id = '#' + slider_id;
	this.speed = speed;
	this.mask = mask; //размер видимой области в px
	this.step = step; //шаг при слайде в px
	this.active_elements = active_elements; //количество видимых элементов
	this.element_counts = null; //количество элементов в слайдере
	this.element_first = null; //индекс первого видисого элемента
	this.element_last = null; //индекс последнего видимого элемента
	this.move = 0;//флаг движения слайдера
	this.nav = indicator;//флаг интдикаторов слайдов
	
	
	this.getSliderId = function(){
		return this.slider_id;
	};
	
	this.getStep = function()
	{
		return this.step;
	};
	
	this.getActiveElements = function()
	{
		return this.activeElements;
	};
	
	this.getElements = function()
	{
		return this.element_counts;
	};
	

	
	this.setElements = function(value)
	{
		if (this.element_counts == null){
			this.element_counts = $(this.slider_id + ' .slider_content > .slide').size();	
		}else{
			this.element_counts = value;
		}
		
		if(this.element_counts <= this.active_elements){
			this.hideLeftArrow();
			this.hideRightArrow();
		}
	};
	
	this.hideLeftArrow = function()
	{
		$(this.slider_id + ' .slider_navigation .arrow_left').toggle('fast');
	};
	
	this.hideRightArrow = function()
	{
		$(this.slider_id + ' .slider_navigation .arrow_right').toggle('fast');
	};
	
	this.getPositionFirst = function()
	{
		return this.element_first;
	};
	
	
	this.setPositionFirst = function(value)
	{
		if (this.element_first == null){
			this.element_first = $(this.slider_id + ' .slider_content .slide.show:first').index();	
		}else{
			this.element_first = value;
		}
	};
	
	this.getPositionLast = function()
	{
		return this.element_last;
	};
	
	this.setPositionLast = function(value)
	{
		if (this.element_last == null){
			this.element_last = $(this.slider_id + ' .slider_content .slide.show:last').index();	
		}else{
			this.element_last = value;
		}
	};
	
	this.s = function(){
		alert(1);
	}
	
	this.moveLeft = function()
	{
		if(this.move == 0)
		{
			this.move = 1;
			
			if(this.element_last  == this.element_counts -1){
				this.firstToLast();
			}
			for (i = this.element_first+1, j = 0; i <= (this.element_last + 2); i++, j+=this.step)
			{
				if(i == (this.element_last + 2)){
					var self = this;
					$(this.slider_id + ' .slider_content .slide:nth-child(' + i + ')').animate({
						left: (j - this.step + 'px')
						}, 
						this.speed, 
						'swing',
						function(){
							self.move = 0;
						}
					);
				}else{
					$(this.slider_id + ' .slider_content .slide:nth-child(' + i + ')').animate({
						left: (j - this.step + 'px')
						}, 
						this.speed,
						'swing'
					);
				}
			}
			$(this.slider_id + ' .slider_content .slide.show:first').removeClass('show');
			$(this.slider_id + ' .slider_content .slide.show:last').next().addClass('show');
				
			this.setPositionFirst(this.element_first + 1);
			this.setPositionLast(this.element_last + 1);
			this.navActive('left');
		}
	};
	
	
	
	this.firstToLast = function()
	{
		$(this.slider_id + ' .slider_content').append($(this.slider_id + ' .slider_content .slide:first'));
		$(this.slider_id + ' .slider_content .slide:last').css('left', this.step*this.active_elements);
		this.setPositionFirst(this.element_first - 1);
		this.setPositionLast(this.element_last - 1);
	};
	
	this.moveRight = function()
	{
		if (this.move == 0)
		{
			this.move = 1;
	
			if(this.element_first  == 0){
				this.lastToFirst();
			}
			for (i = this.element_first-1, j = this.step*(-1); i <= (this.element_last+1 ); i++, j+=this.step)
			{
				if(i == (this.element_last)){
					var self = this;
					$(this.slider_id + ' .slider_content .slide:nth-child(' + i + ')').animate({
						left: (j  + 'px')
						}, 
						this.speed, 
						'swing',
						function(){
							self.move = 0;
						}
					);
				}else{
					$(this.slider_id + ' .slider_content .slide:nth-child(' + i + ')').animate({
						left: (j +  'px')
						}, 
						this.speed,
						'swing'
					);
				}
			}
			$(this.slider_id + ' .slider_content .slide.show:last').removeClass('show');
			$(this.slider_id + ' .slider_content .slide.show:first').prev().addClass('show');
				
			this.setPositionFirst(this.element_first - 1);
			this.setPositionLast(this.element_last - 1);
			this.navActive('right');
		}
	};
	
	this.lastToFirst = function()
	{
		$(this.slider_id + ' .slider_content').prepend($(this.slider_id + ' .slider_content .slide:last'));
		$(this.slider_id + ' .slider_content .slide:first').css('left', this.step*(-1));
		this.setPositionFirst(this.element_first + 1);
		this.setPositionLast(this.element_last + 1);
	};

	this.renderNav = function(){
		if ( this.nav === 1){
			var nav = $('<div>', {
				id: 'sl_navigate',
				class: 'sl_navigate'
			});
			var nav_content = $('<div>', {
				class: 'sl_content'
			});
			
			$(this.slider_id + ' .slider_mask').append(nav);
			$(this.slider_id + ' .sl_navigate').append(nav_content);
			for ( var i=1; i <= this.element_counts; i++){
				var nav_iden = $('<div>', {
					class: 'iden'
				});
				$(this.slider_id + ' .sl_content').append(nav_iden);
				if ( i == 1  ){
					$(this.slider_id + ' .sl_content .iden').addClass('active');
				}
			}
			
		}
	}

	this.navActive = function(triggers) {
		var active_index = $(this.slider_id + ' .sl_navigate .active').index();
		$(this.slider_id + ' .sl_navigate .active').removeClass('active');
		if ( triggers == 'right' ){

			if ( active_index == 0 ){
				$(this.slider_id + ' .sl_navigate .iden:last-child').addClass('active');
			}else{
				$(this.slider_id + ' .sl_navigate .iden:nth-child(' + (active_index) + ')').addClass('active');
			}
		}
		if ( triggers == 'left' ){
			if ( active_index == (this.element_counts-1) ){
				$(this.slider_id + ' .sl_navigate .iden:first-child').addClass('active');
			}else{
				active_index += 1; 
				$(this.slider_id + ' .sl_navigate .iden:nth-child(' + (active_index+1) + ')').addClass('active')
			}
		}
	}
	
	this.setElements(null);
	this.setPositionFirst(null);
	this.setPositionLast(null);
	this.renderNav();
	
	$(this.slider_id).on('click', '.arrow_left', function(){
		if(typeof myTimer != "undefined"){
			clearInterval(myTimer);
		}
		myslider.moveRight();
	});

	$(this.slider_id).on('click', '.arrow_right', function(){
		if(typeof myTimer != "undefined"){
			clearInterval(myTimer);
		}
		myslider.moveLeft();
	});
}
