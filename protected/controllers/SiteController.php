<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->select=1;
		$slides=Slide::getSlides();
		$article = Article::getArticleById(1);
		$this->title=$article->__get('title_lang'.$this->lang);
		$this->render('index', array('slides'=>$slides,
				'article' => $article,
		));
		
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionGetPictures($type=null, $master=null)
	{
		$products=Product::getProducts2( $type, $master);
		$ttt = CJSON::encode($products);
		echo $ttt;
		return 0;
	}
	
	public function actionArtists(){
		$this->select=3;
		$this->title="Мастера";
		$masters=Category::getMasters();
		$this->render('artists', array('masters'=>$masters));
	}
	
	public function actionPhotos($type=null, $page=1){
		$this->select=2;
		if($type=='tatoo'){
			$type=1;
		};
		if($type=='permanent'){
			$type=2;
		};
		if($type=='flash'){
			$type=3;
		}
		$masters = Category::getMasters();
		$products=Product::getProducts($page, $type);
		$this->render('photos', array('products'=>$products, 'type'=>$type, 'masters'=>$masters));
	}
	
	public function actionMaster($page=1, $alias=null){
		$this->select=3;
		$category=Category::getByAlias($alias);
		$masters = Category::getMasters();
		$photos = Product::getProducts($page, null, $category->id);
		//echo coun
		$this->render('master', array('page'=>$page ,'master'=>$category, 'products'=>$photos, 'masters'=>$masters));
	}
	
	public function actionLang ($lang=1){
		Yii::app()->request->cookies['lang']= new CHttpCookie('lang', $lang);
		$this->redirect(Yii::app()->request->urlReferrer);
	}
	
	public function actionContacts() {
		$this->select=5;
		$contacts=Contacts::model()->find(1);
		$days=Days::model()->find(1);
		$params=Params::model()->findAll();
		$this->render('contacts', array('contacts'=>$contacts, 'days'=>$days, 'params'=>$params));
	}
	
	public function actionInfo () {
		$this->select=4;
		$articles=Article::model()->findAll();
		$this->render('articles',array('articles'=>$articles));
	}
}