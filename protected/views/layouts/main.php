
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><?php echo $this->title?></title>
	
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/common/reset.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/content/pager.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/content/article.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/content/artist.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/content/slider.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/content/page_title.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/content/product_images.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/layouts/back_to_top.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/layouts/common.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/layouts/header.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/layouts/footer.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/layouts/left_menu.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/layouts/modal.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/layouts/scroll_div.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/content/contacts.css" type="text/css" media="screen, projection">
	<script src="<?php echo Yii::app()->baseUrl?>/js/libs/jquery/jquery.min.js"></script> 
</head>
<body>
		<div id="wrapper">
			<div id="wrapper_header">
				<div class="fix">
					<div class="mask"></div>
					<div id="header">
						<div class="conteiner">
							<div class="left">
								<ul class="left_menu">
									<li class="level1 <?php if($this->select==1) echo 'active'?>"><a href="<?php echo Yii::app()->baseUrl?>"><?php echo $this->words[0]->__get('lang'.$this->lang)?></a></li>
									<li class="level1 <?php if($this->select==2) echo 'active'?>">
										<a href="<?php echo Yii::app()->baseUrl?>/what-we-do/"><?php echo $this->words[1]->__get('lang'.$this->lang)?></a>
											<div class="arrow">
												<ul class="level2">
												<li><a href="<?php echo Yii::app()->baseUrl?>/what-we-do/tatoo/">tatoo</a></li>
												<li><a href="<?php echo Yii::app()->baseUrl?>/what-we-do/permanent/">permanent</a></li>
												<li><a href="<?php echo Yii::app()->baseUrl?>/what-we-do/flash/">flash</a></li>
											</ul>
										</div>
										
									</li>
									<li class="level1 <?php if($this->select==3) echo 'active'?>"><a href="<?php echo Yii::app()->baseUrl?>/artists"><?php echo $this->words[2]->__get('lang'.$this->lang)?></a></li>
									<li class="level1 <?php if($this->select==4) echo 'active'?>"><a href="<?php echo Yii::app()->baseUrl?>/info/"><?php echo $this->words[3]->__get('lang'.$this->lang)?></a></li>
									<li class="level1 <?php if($this->select==5) echo 'active'?>"><a href="<?php echo Yii::app()->baseUrl?>/contacts/"><?php echo $this->words[4]->__get('lang'.$this->lang)?></a></li>
								</ul>
							</div>
							<div class="logo">
								<div>
									<a href="#" title="На главную">
										<img alt="logo" src="<?php echo Yii::app()->baseUrl?>/images/city8_tattoo_logo.png" />
									</a>
								</div>
							</div>
							<div class="right">
								<div class="conteiner">
									<nav class="language">
										<a <?php if($this->lang==1) echo 'class="active" '; ?>href="<?php echo Yii::app()->baseUrl?>/lang/1">РУС</a>
										<a <?php if($this->lang==2) echo 'class="active" '; ?>href="<?php echo Yii::app()->baseUrl?>/lang/2">ENG</a>
										<a <?php if($this->lang==3) echo 'class="active" '; ?>href="<?php echo Yii::app()->baseUrl?>/lang/3">БЕЛ</a>
									</nav>
								</div>
								<div class="conteiner">
									<nav class="soc">
										<a class="vk" href="#"></a>
										<a class="fb" href="#"></a>
										<a class="tw" href="#"></a>
										<a class="gp" href="#"></a>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="wrapper_content">
				
				<?php echo $content;?>
			</div>
		</div>

	<script type="text/javascript"  src="<?php echo Yii::app()->baseUrl?>/js/backtotop.js"></script>
	<script type="text/javascript"  src="<?php echo Yii::app()->baseUrl?>/js/slider.js"></script>
</body>
</html>
