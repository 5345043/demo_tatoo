-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 01 2013 г., 22:35
-- Версия сервера: 5.5.31
-- Версия PHP: 5.4.4-14+deb7u2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `tatoo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_lang1` varchar(50) NOT NULL,
  `name_lang2` text NOT NULL,
  `name_lang3` text NOT NULL,
  `blog_id` int(11) NOT NULL,
  `pre_text` text NOT NULL,
  `main_text_lang1` text NOT NULL,
  `create_date` date NOT NULL,
  `show` tinyint(1) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `description_meta_lang1` text NOT NULL,
  `description_meta_lang2` text NOT NULL,
  `description_meta_lang3` text NOT NULL,
  `title_lang1` varchar(200) NOT NULL,
  `title_lang2` varchar(200) NOT NULL,
  `title_lang3` varchar(200) NOT NULL,
  `keywoards` varchar(200) NOT NULL,
  `main_text_lang2` text NOT NULL,
  `main_text_lang3` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `article`
--

INSERT INTO `article` (`id`, `name_lang1`, `name_lang2`, `name_lang3`, `blog_id`, `pre_text`, `main_text_lang1`, `create_date`, `show`, `alias`, `description_meta_lang1`, `description_meta_lang2`, `description_meta_lang3`, `title_lang1`, `title_lang2`, `title_lang3`, `keywoards`, `main_text_lang2`, `main_text_lang3`) VALUES
(1, 'Super tatoo', 'reger', '324324', 1, '', '<div style="font-style: normal; font-variant: normal; font-weight: normal; line-height: normal; text-align: center;"><font face="Tahoma">super tatoo</font></div><div style="font-family: Arial, Verdana;"><div style="text-align: center;"><br></div><div style="font-family: Arial, Verdana; font-style: normal; font-variant: normal; font-weight: normal; line-height: normal; text-align: center;"><br></div></div>', '2013-03-12', 1, 'welcome', 'rgfregfre', 'gregerge', '23423423', 'Super tatoo', 'regger', '3242342', 'regre', 'ergerger', '324234'),
(2, 'Расценки', '', '', 1, '', '<div style="font-style: normal; font-variant: normal; font-weight: normal; line-height: normal; text-align: center;"><font face="Tahoma">kuykyukuh</font></div><div style="font-family: Arial, Verdana; text-align: left;"><span style="font-family: Tahoma;"><br></span></div><div style="font-family: Arial, Verdana; font-size: 10pt; text-align: left;"><span style="font-family: Tahoma;"><br></span></div>', '2013-03-24', 1, 'price', '', '', '', 'Расценки', '', '', 'Расценки', '', ''),
(13, 'test', 'test', '22222', 1, '', '<span style="text-decoration: underline;">test</span>', '2013-08-10', 1, 'test', '', '', '', '', '', '', '', '<span style="text-decoration: underline;">test</span>', '<span style="text-decoration: underline;">2222</span>'),
(14, 'Главная статья', 'Главная статья', 'Главная статья', 1, '', '<p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed enim metus. Proin aliquet volutpat sodales. Phasellus ac blandit libero. Vestibulum tincidunt cursus enim, in dignissim nisl pellentesque vitae. Etiam porttitor, nunc a scelerisque laoreet, mi nibh mollis neque, non varius nisl tortor sagittis ipsum. Vivamus blandit viverra posuere. Sed iaculis lectus at odio semper tincidunt. Morbi a felis eu mauris tempor sodales. Ut diam lacus, faucibus a tortor eget, luctus auctor nisl. Duis et enim adipiscing, venenatis magna nec, consectetur sapien. Nam ullamcorper in quam at porttitor. Integer vitae pharetra purus, eu venenatis orci. Nulla rutrum tincidunt ipsum, eu vehicula purus aliquet at. Curabitur faucibus eleifend viverra.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Donec eros felis, ornare vel justo a, porta fringilla felis. Proin ultricies a diam eget dignissim. Nullam auctor lacus ut lacinia imperdiet. Nullam lobortis, orci sit amet adipiscing hendrerit, sem turpis tempor orci, a tincidunt risus magna ut elit. Nam sagittis lobortis cursus. Mauris in tellus mauris. Vestibulum consequat nunc quis arcu aliquet aliquet. Vivamus tincidunt auctor ultrices. Cras nunc lorem, malesuada in ornare sed, vulputate et augue. Suspendisse id nisi ligula. Suspendisse ac libero ac ligula vestibulum pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Nulla sed volutpat sapien. Curabitur tincidunt feugiat felis a accumsan. Nullam dictum ac mi a aliquet. Duis vitae leo sit amet ligula rutrum aliquet id sed turpis. Quisque eu vestibulum orci. Mauris ultrices ullamcorper massa iaculis elementum. Phasellus eu erat id mi venenatis vulputate. Donec in facilisis enim, in pellentesque lacus. In posuere dapibus congue. Praesent eu quam leo. Sed ullamcorper metus quis laoreet pellentesque.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Morbi sed nisi lacinia, congue mi id, venenatis nunc. Proin condimentum auctor auctor. Maecenas venenatis convallis tortor. Proin ornare eleifend suscipit. Maecenas interdum a nibh id facilisis. Suspendisse vel quam vel mi sagittis porttitor. Cras tempor dignissim neque, sed fringilla urna pulvinar nec. Proin tempus pretium dapibus. Pellentesque iaculis elit ut tortor rutrum tempor. Integer et sodales metus. Donec eu felis vel nulla dignissim interdum. Nulla vitae laoreet risus. Nullam venenatis risus eros, in dapibus magna tempor vel. Cras augue dolor, convallis ac malesuada at, fringilla vel leo.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Nunc vitae mattis nisl. Sed aliquam sed lectus at sagittis. Nunc et semper nunc. Suspendisse blandit tortor euismod erat rutrum, nec cursus nisl sagittis. Ut pellentesque sed lorem sit amet viverra. Fusce magna elit, aliquam ut varius eget, dapibus dictum nulla. Nunc venenatis lacinia congue. Integer eget urna mollis, rutrum est sed, pulvinar risus. Donec facilisis ligula et diam imperdiet, vitae convallis risus blandit. Vivamus sit amet ullamcorper orci, id suscipit velit.</p>', '2013-08-24', 1, 'main', '', '', '', '', '', '', '', '<p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed enim metus. Proin aliquet volutpat sodales. Phasellus ac blandit libero. Vestibulum tincidunt cursus enim, in dignissim nisl pellentesque vitae. Etiam porttitor, nunc a scelerisque laoreet, mi nibh mollis neque, non varius nisl tortor sagittis ipsum. Vivamus blandit viverra posuere. Sed iaculis lectus at odio semper tincidunt. Morbi a felis eu mauris tempor sodales. Ut diam lacus, faucibus a tortor eget, luctus auctor nisl. Duis et enim adipiscing, venenatis magna nec, consectetur sapien. Nam ullamcorper in quam at porttitor. Integer vitae pharetra purus, eu venenatis orci. Nulla rutrum tincidunt ipsum, eu vehicula purus aliquet at. Curabitur faucibus eleifend viverra.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Donec eros felis, ornare vel justo a, porta fringilla felis. Proin ultricies a diam eget dignissim. Nullam auctor lacus ut lacinia imperdiet. Nullam lobortis, orci sit amet adipiscing hendrerit, sem turpis tempor orci, a tincidunt risus magna ut elit. Nam sagittis lobortis cursus. Mauris in tellus mauris. Vestibulum consequat nunc quis arcu aliquet aliquet. Vivamus tincidunt auctor ultrices. Cras nunc lorem, malesuada in ornare sed, vulputate et augue. Suspendisse id nisi ligula. Suspendisse ac libero ac ligula vestibulum pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Nulla sed volutpat sapien. Curabitur tincidunt feugiat felis a accumsan. Nullam dictum ac mi a aliquet. Duis vitae leo sit amet ligula rutrum aliquet id sed turpis. Quisque eu vestibulum orci. Mauris ultrices ullamcorper massa iaculis elementum. Phasellus eu erat id mi venenatis vulputate. Donec in facilisis enim, in pellentesque lacus. In posuere dapibus congue. Praesent eu quam leo. Sed ullamcorper metus quis laoreet pellentesque.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Morbi sed nisi lacinia, congue mi id, venenatis nunc. Proin condimentum auctor auctor. Maecenas venenatis convallis tortor. Proin ornare eleifend suscipit. Maecenas interdum a nibh id facilisis. Suspendisse vel quam vel mi sagittis porttitor. Cras tempor dignissim neque, sed fringilla urna pulvinar nec. Proin tempus pretium dapibus. Pellentesque iaculis elit ut tortor rutrum tempor. Integer et sodales metus. Donec eu felis vel nulla dignissim interdum. Nulla vitae laoreet risus. Nullam venenatis risus eros, in dapibus magna tempor vel. Cras augue dolor, convallis ac malesuada at, fringilla vel leo.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Nunc vitae mattis nisl. Sed aliquam sed lectus at sagittis. Nunc et semper nunc. Suspendisse blandit tortor euismod erat rutrum, nec cursus nisl sagittis. Ut pellentesque sed lorem sit amet viverra. Fusce magna elit, aliquam ut varius eget, dapibus dictum nulla. Nunc venenatis lacinia congue. Integer eget urna mollis, rutrum est sed, pulvinar risus. Donec facilisis ligula et diam imperdiet, vitae convallis risus blandit. Vivamus sit amet ullamcorper orci, id suscipit velit.</p>', '<p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed enim metus. Proin aliquet volutpat sodales. Phasellus ac blandit libero. Vestibulum tincidunt cursus enim, in dignissim nisl pellentesque vitae. Etiam porttitor, nunc a scelerisque laoreet, mi nibh mollis neque, non varius nisl tortor sagittis ipsum. Vivamus blandit viverra posuere. Sed iaculis lectus at odio semper tincidunt. Morbi a felis eu mauris tempor sodales. Ut diam lacus, faucibus a tortor eget, luctus auctor nisl. Duis et enim adipiscing, venenatis magna nec, consectetur sapien. Nam ullamcorper in quam at porttitor. Integer vitae pharetra purus, eu venenatis orci. Nulla rutrum tincidunt ipsum, eu vehicula purus aliquet at. Curabitur faucibus eleifend viverra.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Donec eros felis, ornare vel justo a, porta fringilla felis. Proin ultricies a diam eget dignissim. Nullam auctor lacus ut lacinia imperdiet. Nullam lobortis, orci sit amet adipiscing hendrerit, sem turpis tempor orci, a tincidunt risus magna ut elit. Nam sagittis lobortis cursus. Mauris in tellus mauris. Vestibulum consequat nunc quis arcu aliquet aliquet. Vivamus tincidunt auctor ultrices. Cras nunc lorem, malesuada in ornare sed, vulputate et augue. Suspendisse id nisi ligula. Suspendisse ac libero ac ligula vestibulum pharetra. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Nulla sed volutpat sapien. Curabitur tincidunt feugiat felis a accumsan. Nullam dictum ac mi a aliquet. Duis vitae leo sit amet ligula rutrum aliquet id sed turpis. Quisque eu vestibulum orci. Mauris ultrices ullamcorper massa iaculis elementum. Phasellus eu erat id mi venenatis vulputate. Donec in facilisis enim, in pellentesque lacus. In posuere dapibus congue. Praesent eu quam leo. Sed ullamcorper metus quis laoreet pellentesque.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Morbi sed nisi lacinia, congue mi id, venenatis nunc. Proin condimentum auctor auctor. Maecenas venenatis convallis tortor. Proin ornare eleifend suscipit. Maecenas interdum a nibh id facilisis. Suspendisse vel quam vel mi sagittis porttitor. Cras tempor dignissim neque, sed fringilla urna pulvinar nec. Proin tempus pretium dapibus. Pellentesque iaculis elit ut tortor rutrum tempor. Integer et sodales metus. Donec eu felis vel nulla dignissim interdum. Nulla vitae laoreet risus. Nullam venenatis risus eros, in dapibus magna tempor vel. Cras augue dolor, convallis ac malesuada at, fringilla vel leo.</p><p style="text-align: justify; font-size: 11px; line-height: 14px; margin: 0px 0px 14px; padding: 0px; font-family: Arial, Helvetica, sans;">Nunc vitae mattis nisl. Sed aliquam sed lectus at sagittis. Nunc et semper nunc. Suspendisse blandit tortor euismod erat rutrum, nec cursus nisl sagittis. Ut pellentesque sed lorem sit amet viverra. Fusce magna elit, aliquam ut varius eget, dapibus dictum nulla. Nunc venenatis lacinia congue. Integer eget urna mollis, rutrum est sed, pulvinar risus. Donec facilisis ligula et diam imperdiet, vitae convallis risus blandit. Vivamus sit amet ullamcorper orci, id suscipit velit.</p>'),
(15, 'ewdfwe', 'ewdfwe', 'ewdfwe', 1, '', 'dewdew', '2013-08-31', 1, '1111', '', '', '', '', '', '', '', 'dewdew', 'dewdew');

-- --------------------------------------------------------

--
-- Структура таблицы `blog`
--

CREATE TABLE IF NOT EXISTS `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `blog`
--

INSERT INTO `blog` (`id`, `name`) VALUES
(1, 'Pages');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog`
--

CREATE TABLE IF NOT EXISTS `catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `seo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `catalog`
--

INSERT INTO `catalog` (`id`, `name`, `url`, `seo_id`) VALUES
(1, 'Мастера', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_lang1` varchar(255) NOT NULL,
  `name_lang2` varchar(255) NOT NULL,
  `name_lang3` varchar(255) NOT NULL,
  `parent_category` int(11) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  `image1` varchar(40) NOT NULL,
  `url` varchar(50) NOT NULL,
  `position` int(11) NOT NULL,
  `description_lang1` text NOT NULL,
  `description_lang2` text NOT NULL,
  `description_lang3` text NOT NULL,
  `show` tinyint(4) NOT NULL,
  `description_meta_lang1` text NOT NULL,
  `description_meta_lang2` text NOT NULL,
  `description_meta_lang3` text NOT NULL,
  `title_lang1` varchar(100) NOT NULL,
  `title_lang2` varchar(100) NOT NULL,
  `title_lang3` varchar(100) NOT NULL,
  `keywoards` varchar(200) NOT NULL,
  `keywoards_lang2` varchar(255) NOT NULL,
  `keywoards_lang3` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name_lang1`, `name_lang2`, `name_lang3`, `parent_category`, `catalog_id`, `image1`, `url`, `position`, `description_lang1`, `description_lang2`, `description_lang3`, `show`, `description_meta_lang1`, `description_meta_lang2`, `description_meta_lang3`, `title_lang1`, `title_lang2`, `title_lang3`, `keywoards`, `keywoards_lang2`, `keywoards_lang3`) VALUES
(30, 'Master 1', 'Master 1 eng', 'Master 1 by', 0, 1, 'D9nmsx16bJBIa48.jpg', 'mastera', 0, 'Super master', '', '', 1, '', '', '', '', '', '', '', '', ''),
(31, 'Master 2', 'Master 2', 'Master 2', 0, 1, '93fnOzmtTALxd6Y.jpg', 'masterb', 0, 'Super master 2', '', '', 1, '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `map` text NOT NULL,
  `text` text NOT NULL,
  `important` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `contacts`
--

INSERT INTO `contacts` (`id`, `map`, `text`, `important`) VALUES
(1, 'ewrewrewthtr', '<div style="text-align: center;"><span style="font-family: Tahoma; font-size: small; text-align: justify;">+375 29 723 19 54 &nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma; font-size: small;">+375 44 593 19 54 &nbsp;</span></div><div><div style="text-align: center; font-family: Arial, Verdana; font-size: 10pt;"><span style="font-family: Tahoma; font-size: small; text-align: start;">hello@say-yes.by</span></div></div>', '<span style="font-size: small; font-family: Tahoma;">Тексты, а также фото товаров магазина "Say Yes Shop" принадлежат свадебному бюро Say Yes. Их использование без предварительной консультации с нами запрещено.</span>');

-- --------------------------------------------------------

--
-- Структура таблицы `days`
--

CREATE TABLE IF NOT EXISTS `days` (
  `one_day_start` varchar(10) NOT NULL,
  `one_day_end` varchar(10) NOT NULL,
  `one_day_doff` tinyint(1) NOT NULL,
  `thow_day_start` varchar(10) NOT NULL,
  `thow_day_end` varchar(10) NOT NULL,
  `thow_day_doff` tinyint(1) NOT NULL,
  `three_day_start` varchar(10) NOT NULL,
  `three_day_end` varchar(10) NOT NULL,
  `three_day_doff` tinyint(1) NOT NULL,
  `four_day_start` varchar(10) NOT NULL,
  `four_day_end` varchar(10) NOT NULL,
  `four_day_doff` tinyint(1) NOT NULL,
  `five_day_start` varchar(10) NOT NULL,
  `five_day_end` varchar(10) NOT NULL,
  `five_day_doff` tinyint(1) NOT NULL,
  `six_day_start` varchar(10) NOT NULL,
  `six_day_end` varchar(10) NOT NULL,
  `six_day_doff` tinyint(1) NOT NULL,
  `seven_day_start` varchar(10) NOT NULL,
  `seven_day_end` varchar(10) NOT NULL,
  `seven_day_doff` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `days`
--

INSERT INTO `days` (`one_day_start`, `one_day_end`, `one_day_doff`, `thow_day_start`, `thow_day_end`, `thow_day_doff`, `three_day_start`, `three_day_end`, `three_day_doff`, `four_day_start`, `four_day_end`, `four_day_doff`, `five_day_start`, `five_day_end`, `five_day_doff`, `six_day_start`, `six_day_end`, `six_day_doff`, `seven_day_start`, `seven_day_end`, `seven_day_doff`, `id`) VALUES
('10:00', 'fgf', 0, '12:00', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, '', '', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  `url` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `file`, `url`) VALUES
(4, '27.jpg', 'http://host1.demo16753.atservers.net/images/files/27.jpg'),
(6, 'FDFjTbGrRow.jpg', 'http://host1.demo16753.atservers.net/images/files/FDFjTbGrRow.jpg'),
(10, '6bfa54a46d854d63bd0562280f07a367.jpg', 'http://host1.demo16753.atservers.net/images/files/6bfa54a46d854d63bd0562280f07a367.jpg'),
(11, '475f0e90bf56d515f7ccf5c6168682d8.jpg', 'http://host1.demo16753.atservers.net/images/files/475f0e90bf56d515f7ccf5c6168682d8.jpg'),
(12, '0c9e3b4a31a48950606b0e505d2f14a1.jpg', 'http://host1.demo16753.atservers.net/images/files/0c9e3b4a31a48950606b0e505d2f14a1.jpg'),
(13, 'e7b38a1739b8bb5da1239ce44a214c03.jpg', 'http://host1.demo16753.atservers.net/images/files/e7b38a1739b8bb5da1239ce44a214c03.jpg'),
(14, '7fe61c1f180a18902acb7093c1cefb1b.jpg', 'http://host1.demo16753.atservers.net/images/files/7fe61c1f180a18902acb7093c1cefb1b.jpg'),
(16, 'c3f9deb0e8660a340fb3c10639b54c15.jpg', 'http://host1.demo16753.atservers.net/images/files/c3f9deb0e8660a340fb3c10639b54c15.jpg'),
(17, 'd3a5626303285b6e59998558b35e0d5a.jpg', 'http://host1.demo16753.atservers.net/images/files/d3a5626303285b6e59998558b35e0d5a.jpg'),
(18, 'a770ddc17fb7021efae8bb1c96bc0e3e.jpg', 'http://host1.demo16753.atservers.net/images/files/a770ddc17fb7021efae8bb1c96bc0e3e.jpg'),
(19, '7zFrdSN0fX4.jpg', 'http://host1.demo16753.atservers.net/images/files/7zFrdSN0fX4.jpg'),
(20, 'qrLQEwTl-9U.jpg', 'http://host1.demo16753.atservers.net/images/files/qrLQEwTl-9U.jpg'),
(22, '3cEo6s7oTfA.jpg', 'http://host1.demo16753.atservers.net/images/files/3cEo6s7oTfA.jpg'),
(23, '4ee6ffcb816add8ef0c20a67aa2e6a3f.jpg', 'http://host1.demo16753.atservers.net/images/files/4ee6ffcb816add8ef0c20a67aa2e6a3f.jpg'),
(26, '5abd821da85380efcc4347a62d768091.jpg', 'http://host1.demo16753.atservers.net/images/files/5abd821da85380efcc4347a62d768091.jpg'),
(27, '26c0d826672c2ce252cd1981e8f5a56e.jpg', 'http://host1.demo16753.atservers.net/images/files/26c0d826672c2ce252cd1981e8f5a56e.jpg'),
(28, '2d3146bd3f97af106f1c1aff11a56f39.jpg', 'http://host1.demo16753.atservers.net/images/files/2d3146bd3f97af106f1c1aff11a56f39.jpg'),
(29, '221a05e95c257b5df869028d932b8f28.jpg', 'http://host1.demo16753.atservers.net/images/files/221a05e95c257b5df869028d932b8f28.jpg'),
(30, '575a05b91e6116ff2a430b4123c59ef3.jpg', 'http://host1.demo16753.atservers.net/images/files/575a05b91e6116ff2a430b4123c59ef3.jpg'),
(31, '02e1f7b0b9bcfb9723540a65118364a7.jpg', 'http://host1.demo16753.atservers.net/images/files/02e1f7b0b9bcfb9723540a65118364a7.jpg'),
(32, '4a39e022057a1f1d07471b1c8dc99610.jpg', 'http://host1.demo16753.atservers.net/images/files/4a39e022057a1f1d07471b1c8dc99610.jpg'),
(33, '47c2f8f623543d4ef38dd586e7555244.jpg', 'http://host1.demo16753.atservers.net/images/files/47c2f8f623543d4ef38dd586e7555244.jpg'),
(34, '31647b1586ad53faa6427f3eab4d2f31.jpg', 'http://host1.demo16753.atservers.net/images/files/31647b1586ad53faa6427f3eab4d2f31.jpg'),
(35, '7d142005eb69f9dcd53521bd6b7e00b5.jpg', 'http://host1.demo16753.atservers.net/images/files/7d142005eb69f9dcd53521bd6b7e00b5.jpg'),
(38, '176bc37df59ee4142d6bac1e68b2716c.jpg', 'http://host1.demo16753.atservers.net/images/files/176bc37df59ee4142d6bac1e68b2716c.jpg'),
(39, 'HkLcXScGUds.jpg', 'http://host1.demo16753.atservers.net/images/files/HkLcXScGUds.jpg'),
(41, 'b2c68fc9046ccb30e67414c02f8597be.jpg', 'http://host1.demo16753.atservers.net/images/files/b2c68fc9046ccb30e67414c02f8597be.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `langs`
--

CREATE TABLE IF NOT EXISTS `langs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `description` varchar(50) NOT NULL,
  `img` varchar(30) NOT NULL,
  `description2` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `langs`
--

INSERT INTO `langs` (`id`, `name`, `description`, `img`, `description2`) VALUES
(1, 'rus', 'Русский', 'russia.png', 'русском'),
(2, 'eng', 'Англиский', 'uk.png', 'английском'),
(3, 'by', 'Белоруский', 'belarus.png', 'белоруском');

-- --------------------------------------------------------

--
-- Структура таблицы `params`
--

CREATE TABLE IF NOT EXISTS `params` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `params`
--

INSERT INTO `params` (`id`, `value`) VALUES
(1, '111@111.ru'),
(2, 'www.vk.com'),
(3, '505 06 07'),
(4, '666 66 66'),
(5, 'https://pinterest.com/sayyesbureau/'),
(6, '1111'),
(7, '3333');

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_lang1` varchar(50) NOT NULL,
  `name_lang2` varchar(255) NOT NULL,
  `name_lang3` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `product_type_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `url` varchar(50) NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  `description_meta` text NOT NULL,
  `title` varchar(100) NOT NULL,
  `top` int(11) NOT NULL,
  `date_create` date NOT NULL,
  `photo_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `name_lang1`, `name_lang2`, `name_lang3`, `description`, `product_type_id`, `category_id`, `order_id`, `url`, `image`, `description_meta`, `title`, `top`, `date_create`, `photo_type`) VALUES
(10, 'foto 1rr', 'foto 1', 'foto 1', '', 1, 30, 0, 'efewfew', 'efewfew.jpg', '', '', 1, '2013-10-16', 1),
(11, 'rth', 'htrhrt', 'etgrtgtr', '', 1, 30, 0, 'trgrtgtr', 'trgrtgtr.jpg', '', '', 1, '2013-08-25', 2),
(32, 'выфвфыв', '', '', '', 1, 30, 0, 'вфывфв', 'вфывфв.JPG', '', '', 1, '0000-00-00', 1),
(33, 'авыавыа', 'авыавыа', '', '', 1, 30, 0, 'авыаывавыаываывавыа', 'авыаывавыаываывавыа.JPG', '', '', 1, '0000-00-00', 1),
(34, 'авыавыавыаываыва', 'авыавыа', 'авыа', '', 1, 30, 0, 'авыа', 'авыа.JPG', '', '', 1, '0000-00-00', 1),
(36, '1', '', '', '', 1, 30, 0, '1', '1.jpg', '', '', 1, '0000-00-00', 1),
(37, '2', '', '', '', 1, 30, 0, '2', '2.jpg', '', '', 1, '0000-00-00', 1),
(38, '3', '', '', '', 1, 30, 0, '3', '3.JPG', '', '', 1, '0000-00-00', 1),
(39, '4', '', '', '', 1, 30, 0, '4', '4.JPG', '', '', 1, '0000-00-00', 1),
(40, '5', '', '', '', 1, 30, 0, '5', '5.jpg', '', '', 1, '0000-00-00', 1),
(41, '6', '', '', '', 1, 30, 0, '6', '6.jpg', '', '', 1, '0000-00-00', 1),
(42, '7', '', '', '', 1, 30, 0, '7', '7.jpg', '', '', 1, '0000-00-00', 1),
(43, '7', '', '', '', 1, 30, 0, '7', '7.jpg', '', '', 1, '0000-00-00', 1),
(44, '8', '', '', '', 1, 30, 0, '8', '8.jpg', '', '', 1, '0000-00-00', 1),
(45, '9', '', '', '', 1, 30, 0, '9', '9.jpg', '', '', 1, '0000-00-00', 1),
(46, '10', '', '', '', 1, 30, 0, '10', '10.jpg', '', '', 1, '0000-00-00', 1),
(47, '11', '', '', '', 1, 30, 0, '11', '11.jpg', '', '', 1, '0000-00-00', 1),
(48, '12', '', '', '', 1, 30, 0, '12', '12.jpg', '', '', 1, '0000-00-00', 1),
(49, '13', '', '', '', 1, 30, 0, '13', '13.jpg', '', '', 1, '0000-00-00', 1),
(50, '14', '', '', '', 1, 30, 0, '14', '14.jpg', '', '', 1, '0000-00-00', 1),
(51, '15', '', '', '', 1, 30, 0, '15', '15.jpg', '', '', 1, '0000-00-00', 1),
(52, '16', '', '', '', 1, 30, 0, '16', '16.jpg', '', '', 1, '0000-00-00', 1),
(53, '17', '', '', '', 1, 30, 0, '17', '17.jpg', '', '', 1, '0000-00-00', 1),
(54, '18', '', '', '', 1, 30, 0, '18', '18.jpg', '', '', 1, '0000-00-00', 1),
(55, '19', '', '', '', 1, 30, 0, '19', '19.JPG', '', '', 1, '0000-00-00', 1),
(56, '20', '', '', '', 1, 30, 0, '20', '20.jpg', '', '', 1, '0000-00-00', 1),
(57, '21', '', '', '', 1, 30, 0, '21', '21.jpg', '', '', 1, '0000-00-00', 1),
(58, '22', '', '', '', 1, 30, 0, '22', '22.jpg', '', '', 1, '0000-00-00', 1),
(59, '23', '', '23', '', 1, 30, 0, '23', '23.JPG', '', '', 1, '0000-00-00', 1),
(60, '24', '', '', '', 1, 30, 0, '24', '24.jpg', '', '', 1, '0000-00-00', 1),
(61, '25', '', '', '', 1, 30, 0, '25', '25.jpg', '', '', 1, '0000-00-00', 1),
(62, '26', '', '', '', 1, 30, 0, '26', '26.jpg', '', '', 1, '0000-00-00', 1),
(63, '27', '', '', '', 1, 30, 0, '27', '27.jpg', '', '', 1, '0000-00-00', 1),
(64, '28', '', '', '', 1, 30, 0, '28', '28.jpg', '', '', 1, '0000-00-00', 1),
(65, '29', '', '', '', 1, 30, 0, '29', '29.JPG', '', '', 1, '0000-00-00', 1),
(66, '30', '', '', '', 1, 30, 0, '30', '30.jpg', '', '', 1, '0000-00-00', 1),
(70, 'rgr', 'rgfr', 'regre', '', 1, 30, 0, '', '58976582.jpg', '', '', 1, '2013-08-31', 1),
(71, 'tyjhty', 'ytjhty', 'tyhjyt', '', 1, 30, 0, '', '88009189.JPG', '', '', 1, '2013-08-31', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product_image`
--

CREATE TABLE IF NOT EXISTS `product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `image_name` varchar(120) NOT NULL,
  `show` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `description_meta` text NOT NULL,
  `keywoards` varchar(100) NOT NULL,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=191 ;

--
-- Дамп данных таблицы `product_image`
--

INSERT INTO `product_image` (`id`, `prod_id`, `image_name`, `show`, `name`, `description`, `description_meta`, `keywoards`, `title`) VALUES
(1, 4, 'rolling-stones_195_1156.jpg', 1, 'roll1', 'roll1', '', '', ''),
(4, 6, 'fdfds.jpg', 1, 'trgrt', '', '', '', ''),
(9, 6, 'fdfds3235.jpg', 1, 'trgrt', 'fvgfdbvfdgjgjygjyg', 'dvfdvfd', 'fdvfd', 'vbfdv'),
(10, 6, 'fdfds4023.jpg', 1, 'trgrt', '', '', '', ''),
(11, 6, 'fdfds9889.jpg', 1, 'trgrt', '', '', '', ''),
(12, 6, 'fdfds3692.jpg', 1, 'trgrt', '', '', '', ''),
(15, 8, 'prod2.jpg', 1, 'prod2', '', '', '', ''),
(22, 9, '444.jpg', 1, 'Мешковина', '', '', '', ''),
(51, 18, 'erferfer.jpg', 1, 'frefre', '', '', '', ''),
(52, 19, 'trhrthgrt.jpg', 1, 'dfghtrgh', '', '', '', ''),
(53, 20, 'kgfkyf.jpg', 1, 'yfkuyfuy', '', '', '', ''),
(62, 29, 'accessoire7.jpg', 1, 'hrthrth', '', '', '', ''),
(64, 31, 'accessoire9.jpg', 1, 'ui;ug;g', '', '', '', ''),
(72, 32, 'accessoire109005.jpg', 1, 'bsrbbtwr', '', '', '', ''),
(74, 34, 'accessoire125559.jpg', 1, 'bwerwer', '', '', '', ''),
(75, 33, 'accessoire115430.jpg', 1, 'vhgvgh', '', '', '', ''),
(76, 30, 'accessoire88826.jpg', 1, 'ukyuyru', '', '', '', ''),
(78, 24, 'accessoire28113.jpg', 1, 'baerbre', '', '', '', ''),
(79, 28, 'accessoire62632.jpg', 1, 'nstrtrj', '', '', '', ''),
(80, 26, 'accessoire48178.jpg', 1, 'bsbrbebrw', '', '', '', ''),
(81, 25, 'accessoire3206.jpg', 1, 'agerge', '', '', '', ''),
(82, 23, 'accessoire14600.jpg', 1, 'brbare', '', '', '', ''),
(83, 27, 'accessoire53269.jpg', 1, 'lruyluyl', '', '', '', ''),
(88, 39, '15489.jpg', 1, 'gwg', '', '', '', ''),
(95, 43, 'meshkovina1037.jpg', 1, 'jknkdf', '', '', '', ''),
(96, 43, 'meshkovina8799.jpg', 1, 'jknkdf', '', '', '', ''),
(99, 42, 'pink6090.jpg', 1, 'stnmst', '', '', '', ''),
(100, 42, 'pink2853.jpg', 1, 'stnmst', '', '', '', ''),
(102, 44, 'hearts183.jpg', 1, 'HEARTS', '', '', '', ''),
(103, 44, 'hearts9102.jpg', 1, 'HEARTS', '', '', '', ''),
(106, 14, 'tolovetocherish6214.jpg', 1, 'TO LOVE & TO CHERISH', '', '', '', ''),
(109, 14, 'tolovetocherish5830.jpg', 1, 'TO LOVE & TO CHERISH', '', '', '', ''),
(111, 14, 'tolovetocherish6967.jpg', 1, 'TO LOVE & TO CHERISH', '', '', '', ''),
(114, 12, 'waya4524.jpg', 1, 'WAYA', '', '', '', ''),
(115, 12, 'waya2141.jpg', 1, 'WAYA', '', '', '', ''),
(116, 13, 'alwaysandforever9243.jpg', 1, 'ALWAYS & FOREVER', '', '', '', ''),
(117, 13, 'alwaysandforever7375.jpg', 1, 'ALWAYS & FOREVER', '', '', '', ''),
(119, 11, 'behappy8305.jpg', 1, 'BE HAPPY. BE LOVING. BE TRUE', '', '', '', ''),
(120, 11, 'behappy5678.jpg', 1, 'BE HAPPY. BE LOVING. BE TRUE', '', '', '', ''),
(121, 10, 'sakura7610.jpg', 1, 'SAKURA', '', '', '', ''),
(122, 10, 'sakura659.jpg', 1, 'SAKURA', '', '', '', ''),
(123, 10, 'sakura6799.jpg', 1, 'SAKURA', '', '', '', ''),
(124, 11, 'behappy6362.jpg', 1, 'BE HAPPY. BE LOVING. BE TRUE', '', '', '', ''),
(125, 12, 'waya300.jpg', 1, 'WAYA', '', '', '', ''),
(126, 13, 'alwaysandforever6304.jpg', 1, 'ALWAYS & FOREVER', '', '', '', ''),
(136, 48, 'circletofflowers.jpg', 1, 'CIRCLET OF FLOWERS', '', '', '', ''),
(141, 37, 'accessoire154800.jpg', 1, '♥ 15 ♥', '', '', '', ''),
(142, 37, 'accessoire154875.jpg', 1, '♥ 15 ♥', '', '', '', ''),
(143, 38, 'accessoire165408.jpg', 1, '♥ 16 ♥', '', '', '', ''),
(144, 36, 'accessoire141335.jpg', 1, '♥ 14 ♥', '', '', '', ''),
(145, 36, 'accessoire147567.jpg', 1, '♥ 14 ♥', '', '', '', ''),
(146, 35, 'accessoire133335.jpg', 1, '♥ 13 ♥', '', '', '', ''),
(147, 35, 'accessoire134196.jpg', 1, '♥ 13 ♥', '', '', '', ''),
(163, 41, 'bordo695.jpg', 1, 'BORDO', '', '', '', ''),
(168, 16, 'heart5338.png', 1, 'HEART', '', '', '', ''),
(169, 15, 'circletofflowers7601.png', 1, 'LOVE LEAVES white', '', '', '', ''),
(170, 7, 'loveleaves3485.png', 1, 'LOVE LEAVES green', '', '', '', ''),
(172, 17, 'REDCAR928.png', 1, 'RED CAR', '', '', '', ''),
(174, 21, 'birdsgreen8336.png', 1, 'BIRDS green', '', '', '', ''),
(176, 22, 'birdsgrey7540.png', 1, 'BIRDS grey', '', '', '', ''),
(178, 49, 'flowers15345.png', 1, 'FLOWERS', '', '', '', ''),
(180, 45, 'silhouettes239.png', 1, 'SILHOUETTES green', '', '', '', ''),
(182, 46, 'silhouettesred8976.png', 1, 'SILHOUETTES red', '', '', '', ''),
(184, 47, 'silhouettesyellow6958.png', 1, 'SILHOUETTES yellow', '', '', '', ''),
(185, 2, 'dver.jpg', 1, 'Дверт', '', '', '', ''),
(186, 1, 'dver.jpg', 1, 'Дверт', '', '', '', ''),
(187, 6, 'trhyryrt.jpg', 1, 'adolf tatoo', '', '', '', ''),
(188, 7, 'werwerew.jpg', 1, 'ewrew', '', '', '', ''),
(189, 9, 'foto-1.jpg', 1, 'foto 1', '', '', '', ''),
(190, 10, 'efewfew.jpg', 1, 'foto 1', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `product_type`
--

CREATE TABLE IF NOT EXISTS `product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `html_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `product_type`
--

INSERT INTO `product_type` (`id`, `name`, `html_description`) VALUES
(1, 'Аудио альбом', ''),
(2, 'Танк', '');

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `text` text NOT NULL,
  `show` tinyint(1) NOT NULL,
  `description_meta` text NOT NULL,
  `keywoards` varchar(200) NOT NULL,
  `title` varchar(100) NOT NULL,
  `sort` smallint(6) NOT NULL,
  `url` varchar(200) NOT NULL,
  `image1` varchar(100) NOT NULL,
  `image2` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`id`, `name`, `text`, `show`, `description_meta`, `keywoards`, `title`, `sort`, `url`, `image1`, `image2`) VALUES
(1, 'КОНЦЕПЦИЯ СВАДЬБЫ', '<div style="text-align: center;"><span style="font-family: Tahoma;">Разработка концепции свадьбы - этап, когда создается "эскиз" Вашей свадьбы. Основные наши задачи - создать сценарий проведения праздника, определить его стилистику, а также задать особое настроение. Поэтому мы встречаемся с каждой парой, общаемся, изучаем интересы жениха и невесты, их хобби, пристрастия, а также выслушиваем все их пожелания и мечтания.  Далее мы подготавливаем для пары несколько свадебных предложений. Остановившись на конкретном, начинаем тщательную подготовку к свадебному торжеству.</span></div><div style="text-align: center;"><p class="MsoNormal" align="center"></p><p class="MsoNormal" align="center" style="text-align: center;"><img src="http://host1.demo16753.atservers.net/images/files/176bc37df59ee4142d6bac1e68b2716c.jpg" style="font-size: 10pt;"></p><p class="MsoNormal" align="center" style="font-size: 10pt; font-family: Arial, Verdana; text-align: center;"><span style="font-family: Tahoma;"><br></span></p><p style="font-size: 10pt; font-family: Arial, Verdana;"></p></div>', 1, '', 'концепция свадьбы', 'Концепция свадьбы', 1, 'koncepcia', 'xf9y0T7VdqQw2Eh.jpg', 'o75pVhUTnaqtj2J.png'),
(5, 'МЕСТО БАНКЕТА', '<div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">После того, как Вы определитесь с датой торжества и количеством гостей, в первую очередь необходимо сделать выбор -  где будет проходить само торжество. И это один самых ответственных моментов, т.к.  нужно учесть свободен ли на эту дату ресторан, его интерьер, цветовую гамму, возможную рассадку гостей,  изучить меню, уровень обслуживания, уточнить можно ли приносить свои напитки и многое-многое другое!&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">Помимо ресторана в теплое время года торжество может отмечаться  на природе. Например, в шатре с выездным обслуживанием или в уютной усадьбе.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Сделать правильный выбор Вам поможет свадебное бюро <span style="font-weight: bold;">Say Yes</span>!&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="font-weight: normal; text-align: center;"><img src="http://host1.demo16753.atservers.net/images/files/a770ddc17fb7021efae8bb1c96bc0e3e.jpg"></div><div style="font-weight: normal; text-align: center;"><br></div>', 1, '', 'место, банкет', 'Место банкета', 2, 'mesto', 'ZgHYeBI1A6kbqGV.jpg', '0QEjm2rx8hJt7CG.png'),
(6, 'ВЫЕЗДНАЯ РЕГИСТРАЦИЯ', '<div style="text-align: center;"><span style="font-family: Tahoma;">&nbsp;Выездная регистрация - пожалуй один из самых привлекательных способов подчеркнуть индивидуальность события. Декор площадки выездной регистрации может быть оформлен в одной концепции свадьбы, тематике, а также в одном цветовом решении.</span></div><div style="line-height: normal;"><div style="text-align: center;"><span style="line-height: 115%; font-family: Tahoma;">Всем приглашенным запомнятся волнующие минуты обмена\r\nкольцами в знак согласия под воздушным шатром или цветочной аркой, а может быть\r\nпод звездным куполом на крыше высотного здания?&nbsp;</span></div></div><div style="text-align: center;"><span style="font-family: Tahoma; line-height: 18px;">&nbsp;Несомненным плюсом выездной регистрации является и то, что Вы не зависите от ЗАГСа: Вы можете сами планировать дату своей свадьбы, приглашать на церемонию неограниченное количество человек, следовать своему сценарию.</span></div><div style="text-align: center;"><span style="line-height: 115%; font-family: Tahoma;">Ваши предпочтения, наша фантазия и умение обеспечат прекрасную обстановку таким желанным словам из уст\r\nвлюбленных!</span></div><div style="text-align: center;"><span style="line-height: 115%; font-family: Tahoma;"><br></span></div><div style="text-align: center;">&nbsp; &nbsp;&nbsp;<img src="http://host1.demo16753.atservers.net/images/files/475f0e90bf56d515f7ccf5c6168682d8.jpg" style="font-size: 10pt;">&nbsp;&nbsp;<img src="http://host1.demo16753.atservers.net/images/files/6bfa54a46d854d63bd0562280f07a367.jpg" style="font-size: 10pt;"></div>', 1, '', '', 'Выездная регистрация', 3, 'registracia', 'S3qtlbhNZsHJRcp.jpg', 'jO6UaCSMrZF8K4e.png'),
(7, 'СВАДЕБНЫЙ ДЕКОР', '<div style="text-align: center;"><span style="font-family: Tahoma;">Где бы ни проходило торжество, стильное оформление создаст особую атмосферу праздника, который станет главным в Вашей жизни. Можно спланировать грандиозное свадебное торжество, но без должного внимания к деталям даже самая оригинальная идея рискует стать провальной.&nbsp;</span><span style="font-family: Tahoma;">Этого можно избежать, обратившись в свадебное бюро </span><span style="font-family: Tahoma; font-weight: bold;">«Say Yes»</span><span style="font-family: Tahoma;">.</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">&nbsp;Мы учтем все пожелания, воссоздадим целостный образ, не упустив ни одной мелочи. Наши дизайнеры следят за всеми актуальными тенденциями в мире свадебной моды и помогут создать праздник действительно ярким, уникальным и запоминающимся.</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">Если Вы сомневаетесь в своей задумке, мы с удовольствием предложим несколько вариантов на выбор. Начав  работу с определения общей концепции торжества и выбора красочного декора, мы займемся его детальной проработкой и воплощением  в реальность.&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="font-weight: normal; text-align: center;"><img src="http://host1.demo16753.atservers.net/images/files/e7b38a1739b8bb5da1239ce44a214c03.jpg"></div>', 1, 'Свадебный декор', 'Свадебный декор', 'Свадебный декор', 4, 'decor', 'vYGEqKtoFZT9bI4.jpg', 'QIYqaVF1nEz7c3i.png'),
(8, 'СВАДЕБНАЯ ФЛОРИСТИКА', '<div style="text-align: center;"><span style="font-family: Tahoma;">Говоря о флористике, первое, что мы представляем - живые цветы, руки профессионала, привносящие естественную красоту в создаваемый шедевр и прекрасное произведение по завершению. Говоря о свадебной флористике, полет для фантазии бесконечен. Это и букет невесты, бутоньерка, украшение банкетного зала,  букетики для подружек невесты, оформление свадебного кортежа, выездной регистрации.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Все эти составляющие свадебной флористики сейчас актуальны как никогда, т.к. ничто не украшает и не создает атмосферу праздника как цветочные композиции.&nbsp;\r\n</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Наши флористы подберут цветы в гармонии с Вашем стилем и декоративными украшениями, создадут утонченное или пышное цветочное оформление.</span><br></div><div style="text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="text-align: center;"><img src="http://host1.demo16753.atservers.net/images/files/d3a5626303285b6e59998558b35e0d5a.jpg"></div>', 1, 'СВАДЕБНАЯ ФЛОРИСТИКА', 'СВАДЕБНАЯ ФЛОРИСТИКА', 'Свадебная флористика', 5, 'floristika', 'BJ4pZtAiuzw0Lln.jpg', '4nUaEXgORBQjPuW.png'),
(9, 'ФОТОСЪЁМКА', '<div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">«Остановись, мгновенье, ты прекрасно!» - лучшего девиза для талантливых фотографов не придумали до сих пор. Свадебные фото - прекрасные мгновения, пойманные цепким взглядом фотографа и сохраненные для Вас с помощью совершенного оборудования.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Фотосъёмка - дело требующее большого профессионализма и ответственности. Поэтому первостепенными пунктами для нас является качественный отбор мастеров фотографии. Чтобы Вы, беря в руки готовые работы, с улыбкой на лице и и радостью в глазах, вспоминали такой важный и блестящий для обоих день. Мы всегда сможем предложить Вам различные варианты и места для съемок в городе или за его пределами в зависимости от пожеланий.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Обращаясь к свадебному бюро <span style="font-weight: bold;">"Say Yes"</span>, получаете гарантию быстрого оформления семейного альбома, и такой желанный момент повернуть время вспять, рассматривая притягательную и яркую историю. </span></div><div style="text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="text-align: center;"><img src="http://host1.demo16753.atservers.net/images/files/7zFrdSN0fX4.jpg">&nbsp;&nbsp;<img src="http://host1.demo16753.atservers.net/images/files/qrLQEwTl-9U.jpg" style="font-size: 10pt;"></div>', 1, 'ФОТОСЪЁМКА', 'ФОТОСЪЁМКА', 'Фотосъемка', 6, 'photo', 'SP4C69Jg1KtWjMz.jpg', 'bE0XphyiBMkl6eo.png'),
(10, 'ВИДЕОСЪЁМКА', '<div style="text-align: center;"><span style="font-family: Tahoma;">А другими словами &nbsp;маленькое кино о вашей любви, которое хочется пересматривать и пересматривать с годами.</span><span style="font-family: Tahoma;">Любое кино должно быть снято профессионально. То же самое мы можем сказать и о вашем свадебном видео. От профессионально-художественного взгляда видеографа зависит, какой запомнится ваша свадьба вам и вашим потомкам.</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Наши видеоператоры снимают на самую современную аппаратуру (Canon Mark II и Mark III), что позволяет видео приобретать кинематографическое качество изображения.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="text-align: center;"><br></div>', 1, 'Видеосъемка', 'Видеосъемка', 'Видеосъемка', 7, 'video', 'NvxS9KYFeb10Roc.jpg', 'PuDLeqfIZBkW73H.png'),
(11, 'СВАДЕБНАЯ ПОЛИГРАФИЯ', '<div style="text-align: center;"><span style="font-family: Tahoma;">Свадебная полиграфия заслуживает не меньшего внимания, чем выбор свадебного платья для невесты. Она поможет сделать свадьбу запоминающейся и оригинальной.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Использование приглашений, открыток, рассадных табличек, меню, банкетных карточек, номерков на столах, изготовление фотокниг, альбомов для пожеланий даст Вам возможность продемонстрировать тонкий вкус, к тому же,  это считается признаком хорошего тона.&nbsp;</span><span style="font-family: Tahoma;">Ведь внимание к деталям - это не только красиво, но и удобно.&nbsp;</span><span style="font-family: Tahoma;">А вкладыши со схемой проезда, вложенные в пригласительные, помогут Вашим гостям добраться до места проведения торжества.&nbsp;</span><span style="font-family: Tahoma;">&nbsp;Приглашения, разработанные специально для Вас, станут не только визитной карточкой мероприятия, но и будут долго храниться у гостей, навевая воспоминания о чудесном времени. Материал, форма, дизайн выбирается с учетом предпочтений и ведущих свадебных тенденций. Будь это классика, винтажная стилистика, следование единой тематике или ретро-фото-приглашения.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Мы с удовольствием поможем Вам создать неповторимый стиль этих необыкновенно важных мелочей и наполнить их чудесными авторскими текстами!</span></div><div style="text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="text-align: center;"><img src="http://host1.demo16753.atservers.net/images/files/4ee6ffcb816add8ef0c20a67aa2e6a3f.jpg"></div>', 1, 'Свадебная Полиграфия', 'Свадебная Полиграфия', 'Свадебная Полиграфия', 8, 'poligrafia', 'Ksagpck5zr6HN3n.jpg', 'LGN9JpYZM58FKRd.png'),
(12, 'ПРИЧЁСКА И МАКИЯЖ', '<div style="text-align: center;"><span style="font-family: Tahoma;">В торжественный день своего бракосочетания каждая невеста мечтает быть неотразимой. Свадебная прическа и макияж - один из главных этапов в подготовке планируемого торжества. И абсолютно не важно, планируется ли множество гостей или молодожены захотят отметить рождение своей семьи вдвоём, макияж на свадьбу и укладка должны  быть безупречной.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Наши мастера помогут Вам подобрать идеальный образ и стиль, дадут советы и расскажут все об особенностях свадебного макияжа.</span></div><div style="text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="text-align: center;"><img src="http://host1.demo16753.atservers.net/images/files/2d3146bd3f97af106f1c1aff11a56f39.jpg" style="font-size: 10pt;">&nbsp; &nbsp;<img src="http://host1.demo16753.atservers.net/images/files/26c0d826672c2ce252cd1981e8f5a56e.jpg" style="font-size: 10pt;"></div>', 1, '', '', 'Причека и макияж', 9, 'pricheska', 'wJOcQRzKD4WG57x.jpg', 'xjRvMBlzYHfAaNF.png'),
(13, 'ВЕДУЩИЙ', '<div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">Хороший ведущий - тот, кто умеет задать свадьбе эмоциональный тон и создать атмосферу красивого, романтичного, яркого праздника.&nbsp;</span><span style="font-family: Tahoma;">Первые ассоциации,  возникающие мгновенно при упоминании ведущего свадебного вечера - «тамада». Лично нам формат тамады не очень близок (хотя, в своем исконном значении, это почетная и красивая роль, которую нужно заслужить).&nbsp;</span><span style="font-family: Tahoma;">Мы имеем в виду, что нам не интересен персонаж эдакого массовика-затейника.&nbsp;</span><span style="font-family: Tahoma;">Считаем, что ведущий свадьбы не должен становиться центром торжества, перетягивающим внимание с жениха и невесты на себя.&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">Близким к видению этой персоны является отличное определение «конферансье».&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">От него зависит процесс проведения свадьбы, настроение молодоженов и всех приглашенных гостей.&nbsp;</span><span style="font-family: Tahoma;">Конферансье должен ненавязчиво и деликатно направлять за собой праздничное действие, чувствовать аудиторию и дарить приятные эмоции окружающим.&nbsp;</span></div><div style="text-align: center;"><span style="font-weight: normal; font-family: Tahoma;">Поэтому к выбору ведущего нужно подойти серьезно, заблаговременно и ответственно.&nbsp;</span><span style="font-weight: normal; font-family: Tahoma;">Встретившись с кандидатами, проанализируйте чувство собственного комфорта и непосредственности в общении. Если возникнут лишь положительные эмоции, то это Ваш человек, который привнесёт гармонию и слаженность в празднование.&nbsp;</span><span style="font-family: Tahoma;">Свадебное бюро <span style="font-weight: bold;">«Say Yes»</span> подберёт самых проверенных  и опытных профессионалов своего дела, а Ваши гости не раз обратятся к Вам за рекомендацией.</span></div><div style="text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="text-align: center;"><img src="http://host1.demo16753.atservers.net/images/files/221a05e95c257b5df869028d932b8f28.jpg"></div>', 1, 'Ведущий', 'Ведущий', 'Ведущий', 10, 'veduchij', 'upHckt87XeSBzb0.jpg', 'nzxksKhvgAf30Or.png'),
(14, 'ШОУ-ПРОГРАММА', '<div style="text-align: center;"><span style="font-family: Tahoma;">Шоу-программа - один из элементов любой организации праздника, подчеркивающий торжественность и великолепие события.&nbsp;</span><span style="font-family: Tahoma;">Развлекательная часть свадебного банкета - это грамотное соотношение номеров и музыкально-танцевальных блоков, конферанса и интерактива  от ведущего, живого общения и тихих пауз, экспромта и юмора.&nbsp;</span><span style="font-family: Tahoma;">Многие считают, что шоу-программа на свадьбе состоит из тандема ведущего и диджея с чередой конкурсов. Этот подход самый традиционный.&nbsp;</span><span style="font-family: Tahoma;">И как во всем традиционном, есть вероятность уйти в банальность и пресность.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Встречается и другая крайность: банкет в формате концертной программы.&nbsp;</span><span style="font-family: Tahoma;">Это может быть безумно интересно, но если программа слишком насыщенна, она может утомить Ваших гостей.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Именно поэтому, мы ставим перед собой задачу двигаться по нити золотой середины и проводить мероприятие с учетом всех особенностей культурно-досуговой сферы.&nbsp;</span><span style="font-family: Tahoma;">Шоу-программа должна быть согласована со сценарием, в котором официальная часть плавно переходит в легкое веселье развлекательной программы, а официальный тон - в непринужденную дружескую атмосферу.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">От Вас потребуется лишь подробно рассказать нам о том, каким бы Вы хотели видеть свадебное действие, а мы, исходя из пожеланий, разработаем весь сценарий проведения от начала и до конца.</span></div><div style="text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="text-align: center;"><img src="http://host1.demo16753.atservers.net/images/files/575a05b91e6116ff2a430b4123c59ef3.jpg"></div>', 1, 'Шоу-программа', 'Шоу-программа', 'Шоу-программа', 11, 'show', 'h8PTDpKFqb5s9Xz.jpg', 'owdWRQmPtUSugHy.png'),
(15, 'СВАДЕБНЫЙ ТОРТ', '<div style="text-align: center;"><span style="font-family: Tahoma;">Торт - это не просто украшение банкета, это отдельный номер программы. Сладкое произведение искусства и хороший сюрприз для родителей и друзей. Каким он будет - решать Вам. Возможно, вдохновившись западными примерами, молодые предпочтут  удобные  и практичные, при большом количестве приглашенных гостей, капкейки. А может Вашим выбором станет величественный многоярусный торт, украшенный мастикой с замысловатым рисунком? В любом случае, каким вдохновением не будет рождена творческая сладость, она несёт в себе кульминацию свадьбы и самый незабываемый момент. Проконсультировавшись по поводу начинки, внешнего вида и основной детализации, можно смело браться за столь приятный организационный момент!</span></div><div style="text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="text-align: center;"><img src="http://host1.demo16753.atservers.net/images/files/4a39e022057a1f1d07471b1c8dc99610.jpg"></div>', 1, 'Свадебный торт', 'Свадебный торт', 'Свадебный торт', 12, 'tort', '5sTnSdPcyXrNt9H.jpg', 'qZzE3mnoRtC4rwJ.png'),
(16, 'СВАДЕБНЫЙ ТРАНСПОРТ', '<div style="text-align: center;"><span style="font-family: Tahoma;">Любые брачующиеся при планировании своего торжества останавливают свой выбор на шикарном средстве передвижения. Это является не только практично: можно занять гостей, увидеть прекрасные достопримечательности города, и запечатлеть радостные моменты, но и весело. Ведь где, как ни в просторном лимузине, ретро-машине, кабриолете, Вы почувствуете то нисходящее чувство торжества, настраивающее на верный лад всех присутствующих.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Очень важно, чтобы автотранспорт отвечал целому ряду требований: был удобным и стильным, все автомобили входящие в кортеж сочетались друг с другом, как  по цвету  так и по классу, а украшения были тщательно подобраны. Любая невеста, а тем более жених, подлинный ценитель автомобилей, мечтает, чтобы их свадьба была сказочно красивой. Используя возможности и подключив воображение, мы создадим незабываемую и весело-шумную поездку в кругу самых  близких.&nbsp;</span></div><div style="text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="text-align: center;">&nbsp;&nbsp;<img src="http://host1.demo16753.atservers.net/images/files/47c2f8f623543d4ef38dd586e7555244.jpg" style="font-size: 10pt;">&nbsp;<img src="http://host1.demo16753.atservers.net/images/files/31647b1586ad53faa6427f3eab4d2f31.jpg" style="font-size: 10pt;"><span style="font-size: 10pt;">&nbsp;</span></div>', 1, 'Свадебный транспорт', 'Свадебный транспорт', 'Свадебный транспорт', 13, 'transport', '2WuIQSfPgN64apY.jpg', 'zY7rifWsctkwFH3.png'),
(17, 'РАСПОРЯДИТЕЛЬ', '<div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">Свадебное торжество весьма важное событие для всех принимающих в нем непосредственное участие. Планируя мероприятие такого масштаба, очень важно учесть все моменты и нюансы. Свадебный распорядитель из тех важных персон, который не только поможет Вам лавировать во всех вопросах, но и станет направляющим на протяжении всей церемонии.&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">Это человек,  который выполняет все координирующие функции свадьбы, а именно:</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">♥ контролирует прибытие и работу стилиста, фотографа, видеооператора, ведущего, доставку торта;&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">♥  контролирует прибытие и рассадку гостей;&nbsp;\r\n</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">♥ координирует маршрут свадебного кортежа, с учетом непредвиденных ситуций;&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">♥ контролирует оформление банкетного зала&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">♥ контролирует работу сотрудников ресторана: время подачи блюд, свадебного торта и расход напитков;&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">♥ встречает и контролирует выход артистов;&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">♥ решает вопросы и пожелание молодых в течение всего свадебного дня.&nbsp;</span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;"><br></span></div><div style="font-weight: normal; text-align: center;"><span style="font-family: Tahoma;">Другими словами,  свадебный распорядитель  - это Ваш персональный свадебный «помощник».</span></div><div style="text-align: center;"><span style="font-family: Tahoma;">Свадебное бюро<span style="font-weight: bold;"> "Say Yes"</span> не позволит Вам волноваться и переживать за организационные вопросы в день свадьбы. Мы сделаем все так, чтобы Вы только наслаждались своим самым главным днем в жизни, днем рождения новой семьи!&nbsp;</span></div>', 1, '', '', '', 14, 'rasporaditel', 'YrRstMClVE16kUe.jpg', '90Ch8jrRl1mw3Og.png');

-- --------------------------------------------------------

--
-- Структура таблицы `site_admin`
--

CREATE TABLE IF NOT EXISTS `site_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `site_admin`
--

INSERT INTO `site_admin` (`id`, `name`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(200) NOT NULL,
  `url` varchar(300) NOT NULL,
  `show` tinyint(1) NOT NULL,
  `position` smallint(6) NOT NULL,
  `text_lang1` text NOT NULL,
  `text_lang2` text NOT NULL,
  `text_lang3` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Дамп данных таблицы `slider`
--

INSERT INTO `slider` (`id`, `file`, `url`, `show`, `position`, `text_lang1`, `text_lang2`, `text_lang3`) VALUES
(29, '1.jpg', 'gregerger', 1, 3, 'vvvv', 'regregfre', 'regergerger'),
(30, '3.jpg', '', 1, 1, 'hr6hrthtfhrtt', '', ''),
(31, '3.JPG', 'fewfwe', 1, 33, 'efes', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `word`
--

CREATE TABLE IF NOT EXISTS `word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang1` text NOT NULL,
  `lang2` text NOT NULL,
  `lang3` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `word`
--

INSERT INTO `word` (`id`, `lang1`, `lang2`, `lang3`) VALUES
(1, 'Главная', 'Home', 'Галоуная'),
(2, 'Наши работы', 'Wht we do', 'Шо мы робим'),
(3, 'Мастера', 'Artists', 'Майстры'),
(4, 'Инфо', 'Info', 'Инфо'),
(5, 'Контакты', 'Contacts', 'Кантакты'),
(6, 'Понедельник', 'Mondаy', 'Панядзелак'),
(7, 'Вторник', 'Tuesday', 'Ауторак'),
(8, 'Среда', 'Wednesday', 'Серада'),
(9, 'Четверг', 'Thursday', 'Чацьверг'),
(10, 'Пятница', 'Friday', 'Пятница'),
(11, 'Суббота', 'Saturday', 'Субота'),
(12, 'Воскресенье', 'Sunday', 'Нядзеля');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
